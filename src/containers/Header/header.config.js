import React from "react";
import { Exit, Home, User, Setting } from "./components";
import { routesList } from "../../assets/enums";

export const panelAuth = [
  {
    img: <Home />,
    href: routesList.HOME,
    label: "My account",
  },
  {
    img: <User />,
    href: routesList.PROFILE,
    label: "Profile",
  },
  {
    img: <Setting />,
    href: routesList.SETTINGS,
    label: "Settings",
  },
  {
    img: <Exit />,
    href: routesList.SIGN_IN,
    label: "Log out",
    labelIcon: "exit",
    action: "exit",
  },
].map((item, i) => ({ ...item, id: i }));

export const panel = [
  /*
  {
    img: <Enter />,
    href: routesList.HOME,
    labelIcon: "exit",
  },
  {
    img: <Reg />,
    href: routesList.PROFILE,
    label: "Profile",
  },
  */
].map((item, i) => ({ ...item, id: i }));
