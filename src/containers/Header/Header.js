import React, { useEffect, useState } from "react";
import { Link, useHistory, useLocation } from "react-router-dom";
import clsx from "clsx";
import { inject, observer } from "mobx-react";
// import { MySelect } from "../../components/MySelect/MySelect";

// local files
import { Logo, MyTooltip } from "../../components";
import { routesList } from "../../assets/enums";
import { ToggleMenu } from "./components/ToggleMenu/ToggleMenu";
import { panel, panelAuth } from "./header.config";
import { TOKEN } from "../../assets/constans/auth.const";
import "./Header.scss";

export const Header = inject("store")(
  observer(({ store: { dashboard, auth } }) => {
    const { token, info } = auth;

    const userImgDefault = require("./assets/user.svg");

    const history = useHistory();
    const location = useLocation();

    const [isAuth, setIsAuth] = useState(false);
    const [menuOpen, setMenuOpen] = useState(false);

    useEffect(() => {
      if (token && !isAuth) {
        setIsAuth(true);
        auth.getUserInfo();
      }
    }, [token]);

    /*
    const RenderWithImg = ({ label, img }) => {
      return (
        <div
          style={{
            display: "flex",
            flexWrap: "wrap",
            alignItems: "center",
          }}
        >
          <img src={img} alt="" style={{ marginRight: 20 }} />
          <span>{label}</span>
        </div>
      );
    };
    */

    /*
    const langData = [
      {
        label: (
          <RenderWithImg
            label="USD"
            img={require("./assets/currency/USD.png")}
          />
        ),
        value: "usd",
        img: require("./assets/currency/USD.png"),
      },
      {
        label: (
          <RenderWithImg
            label="RUB"
            img={require("./assets/currency/RUB.png")}
          />
        ),
        value: "rub",
        img: require("./assets/currency/RUB.png"),
      },
    ];
    */

    const handleToggleMenu = () => {
      setMenuOpen(!menuOpen);
    };

    const handleCloseMenu = () => {
      setMenuOpen(false);
    };

    const handleExitClick = e => {
      e.preventDefault();
      handleCloseMenu();
      setIsAuth(false);
      auth.setToken(null);
      dashboard.cardsList = [];
      localStorage.removeItem(TOKEN);
      history.push(routesList.SIGN_IN);
    };

    return (
      <header className="c-header">
        {isAuth && (
          <ToggleMenu
            data={panelAuth}
            open={menuOpen}
            onToggle={handleToggleMenu}
            onExitClick={handleExitClick}
          />
        )}
        <div className="container">
          <div
            className={clsx(
              "c-header-inner",
              !isAuth && "c-header-inner--not-auth",
            )}
          >
            {isAuth && (
              <button
                className="c-header__burger default"
                type="button"
                onClick={handleToggleMenu}
              >
                <img src={require("./assets/Menu.svg")} alt="" />
              </button>
            )}
            <div
              className={clsx(
                "c-header__logo-wrap",
                isAuth
                  ? "c-header__logo-wrap--auth"
                  : "c-header__logo-wrap--not-auth",
              )}
            >
              <Logo />
            </div>
            {isAuth && (
              <MyTooltip id="tooltip-profile" value="Profile">
                <Link className="c-profile" to={routesList.PROFILE}>
                  <div
                    className="c-profile__img card-media"
                    style={{ backgroundImage: `url(${userImgDefault})` }}
                  />
                  <div className="c-profile__text">
                    <div className="c-profile__text__name">{info.login}</div>
                    <div className="c-profile__text__email">{info.email}</div>
                  </div>
                </Link>
              </MyTooltip>
            )}
            {/*
            <div
              className={clsx(
                "c-currency",
                isAuth ? "c-currency--auth" : "c-currency--not-auth"
              )}
            >
              <MySelect data={langData} defaultValue={langData[0]} />
            </div>
            */}
            <div
              className={clsx(
                "c-panel",
                isAuth ? "c-panel--auth" : "c-panel--not-auth",
              )}
            >
              {(isAuth ? panelAuth : panel).map(item => (
                <div className="c-panel__item" key={item.id}>
                  <MyTooltip id={`${item.id}`} value={item.label}>
                    <Link
                      onClick={item.action === "exit" ? handleExitClick : null}
                      className={clsx(
                        "c-panel__item__link",
                        location.pathname === item.href && "active",
                      )}
                      to={item.href}
                    >
                      {item.img}
                    </Link>
                  </MyTooltip>
                </div>
              ))}
            </div>
          </div>
        </div>
      </header>
    );
  }),
);
