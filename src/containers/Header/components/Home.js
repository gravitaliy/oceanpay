import React, { memo } from "react";
import "./common.scss";

export const Home = memo(() => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="25"
      x="0"
      y="0"
      enableBackground="new 0 0 72.5 67.3"
      version="1.1"
      viewBox="0 0 72.5 67.3"
      xmlSpace="preserve"
      className="panel-icon"
    >
      <path
        d="M71.5 28.8L42.3 2.3c-3.4-3.1-8.6-3.1-12.1 0L1 28.8c-1.2 1.1-1.3 3-.2 4.3.6.6 1.4 1 2.2 1h3v27.2c0 3.3 2.7 6 6 6h12.1c1.7 0 3-1.4 3-3V46.2c0-1.7 1.4-3 3-3h12.1c1.7 0 3 1.4 3 3v18.1c0 1.7 1.4 3 3 3h12.1c3.3 0 6-2.7 6-6V34.1h3c1.7 0 3-1.4 3-3 .2-.9-.1-1.7-.8-2.3z"
        className="st0"
      />
    </svg>
  );
});
