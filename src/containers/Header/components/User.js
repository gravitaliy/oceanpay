import React, { memo } from "react";
import "./common.scss";

export const User = memo(() => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="19"
      height="25"
      x="0"
      y="0"
      enableBackground="new 0 0 46.9 63.4"
      version="1.1"
      viewBox="0 0 46.9 63.4"
      xmlSpace="preserve"
      className="panel-icon"
    >
      <path
        d="M31.4 42.9c-.1.6-.3 1.1-.5 1.5-1.5 2.5-6 2.7-7.4 2.7-5.3 0-7-2.1-7.4-2.7-.3-.4-.4-.9-.5-1.5-5.7 1.4-10.7 4.2-14.2 8-2.1 2.2-1.6 5.9 1 7.5 5 3.1 11.8 5 21.1 5s16.2-1.9 21.1-5c2.6-1.6 3.1-5.3 1-7.5-3.6-3.8-8.5-6.6-14.2-8z"
        className="st0"
      />
      <path
        d="M38.1 21.4c0-8.1 0-21.4-14.6-21.4S8.8 13.4 8.8 21.4c0 6.5 4.2 12 10 13.9 0 0-1.8 6.1-.8 7.8.7 1.2 3.9 1.5 5.3 1.5s4.6-.4 5.3-1.5c1-1.7-.8-7.8-.8-7.8 6-1.9 10.3-7.4 10.3-13.9z"
        className="st0"
      />
    </svg>
  );
});
