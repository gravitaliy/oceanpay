import React, { memo } from "react";
import { Link, useLocation } from "react-router-dom";
import clsx from "clsx";
import "./ToggleMenu.scss";

export const ToggleMenu = memo(({ data, open, onToggle, onExitClick }) => {
  const location = useLocation();

  return (
    <div className={clsx("toggle-menu", open && "active")}>
      <div className="toggle-menu__head">
        <button className="default" type="button" onClick={onToggle}>
          <img src={require("./assets/Back.svg")} alt="" />
        </button>
        {data.map(
          item =>
            item.labelIcon === "exit" && (
              <Link to={item.href} key={item.id} onClick={onExitClick}>
                <img src={require("./assets/exit.svg")} alt="" />
              </Link>
            ),
        )}
      </div>
      <div className="toggle-menu__list">
        {data.map(
          item =>
            !item.labelIcon && (
              <Link
                to={item.href}
                className={clsx(
                  "default",
                  "toggle-menu__list-item",
                  location.pathname === item.href && "active"
                )}
                key={item.id}
                onClick={onToggle}
              >
                {item.label}
              </Link>
            ),
        )}
      </div>
    </div>
  );
});
