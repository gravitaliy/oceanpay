import React, { memo } from "react";
import "./common.scss";

export const Exit = memo(() => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="25"
      height="26"
      x="0"
      y="0"
      enableBackground="new 0 0 76.2 80.8"
      version="1.1"
      viewBox="0 0 76.2 80.8"
      xmlSpace="preserve"
      className="panel-icon"
    >
      <path
        d="M59.5 23.7L53.9 29.4 60.9 36.4 28.2 36.4 28.2 44.4 60.9 44.4 53.9 51.4 59.5 57.1 76.2 40.4z"
        className="st0"
      />
      <path
        d="M45.3 72.8L8 72.8 8 8 45.3 8 45.3 16.6 53.3 16.6 53.3 0 0 0 0 80.8 53.3 80.8 53.3 64.2 45.3 64.2z"
        className="st0"
      />
    </svg>
  );
});
