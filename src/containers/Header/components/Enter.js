import React, { memo } from "react";
import "./common.scss";

export const Enter = memo(() => {
  return (
    <svg
      width="23"
      height="25"
      xmlns="http://www.w3.org/2000/svg"
      x="0"
      y="0"
      enableBackground="new 0 0 76.2 80.8"
      version="1.1"
      viewBox="0 0 76.2 80.8"
      xmlSpace="preserve"
      className="panel-icon"
    >
      <path
        d="M44.9 57.1L50.5 51.4 43.5 44.4 76.2 44.4 76.2 36.4 43.5 36.4 50.5 29.4 44.9 23.7 28.2 40.4z"
        className="st0"
      />
      <path
        d="M45.3 72.8L8 72.8 8 8 45.3 8 45.3 16.6 53.3 16.6 53.3 0 0 0 0 80.8 53.3 80.8 53.3 64.2 45.3 64.2z"
        className="st0"
      />
    </svg>
  );
});
