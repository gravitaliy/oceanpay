import { decorate, observable } from "mobx";
import "mobx-react-lite/batchingForReactDom";
import { SettingsAction } from "./settings.action";
import { idPasswordConfirmation } from "../../assets/functions";
// local

class SettingsStore extends SettingsAction {
  loading = false;

  inputs = {
    old_password: "",
    new_password: "",
    [idPasswordConfirmation]: "",
  };
}

decorate(SettingsStore, {
  loading: observable,
  inputs: observable,
});

export default new SettingsStore();
