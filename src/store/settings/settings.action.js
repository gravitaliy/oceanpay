import { toast } from "react-toastify";
import { axiosInstanceAuth } from "../../api/axios";
import {
  clearData,
  getDataWithoutPasswordConfirmation,
} from "../../assets/functions";

export class SettingsAction {
  setLoading(state) {
    this.loading = state;
  }

  setValue(name, value) {
    this.inputs[name] = value;
  }

  setInputs(value) {
    this.inputs = value;
  }

  sendSettings() {
    this.setLoading(true);

    axiosInstanceAuth
      .post(
        "/Auth/ChangePassword",
        getDataWithoutPasswordConfirmation(this.inputs),
      )
      .then(
        () => {
          this.setInputs(clearData(this.inputs));
          this.setLoading(false);
          toast.success("The profile has been successfully updated");
        },
        err => {
          this.setLoading(false);
          if (err?.response?.data?.message) {
            toast.error(err?.response?.data?.message);
          }
        },
      );
  }
}
