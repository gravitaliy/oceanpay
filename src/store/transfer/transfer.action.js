import { toast } from "react-toastify";
import { axiosInstance } from "../../api/axios";

export class TransferAction {
  setLoading(state) {
    this.loading = state;
  }

  setValue(name, value) {
    this.inputs[name] = value;
  }

  sendTransfer() {
    this.setLoading(true);

    setTimeout(() => {
      this.setLoading(false);
      toast.success("Money has been successfully sent");
    }, 300);

/*
    axiosInstance.post("/Cryptocard/SendMoney", this.inputs).then(
      () => {
        this.setLoading(false);
        toast.success("Money has been successfully sent");
      },
      (err) => {
        this.setLoading(false);
        if (err?.response?.data?.message) {
          toast.error(err?.response?.data?.message);
        }
      },
    );
*/
  }
}
