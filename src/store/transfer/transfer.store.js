import { decorate, observable } from "mobx";
import "mobx-react-lite/batchingForReactDom";

// local files
import { TransferAction } from "./transfer.action";

class TransferStore extends TransferAction {
  loading = false;

  inputs = {
    from_card_number: "",
    to_card_number: "",
    amount: "",
  };
}

decorate(TransferStore, {
  loading: observable,
  inputs: observable,
});

export default new TransferStore();
