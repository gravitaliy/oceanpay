import DashboardStore from "./dashboard/dashboard.store";
import ReplenishmentStore from "./replenishment/replenishment.store";
import AuthStore from "./auth/auth.store";
import ProfileStore from "./profile/profile.store";
import TransferStore from "./transfer/transfer.store";
import SettingsStore from "./settings/settings.store";
import WalletStore from "./wallet/wallet.store";

class RootStore {
  constructor() {
    this.dashboard = DashboardStore;
    this.replenishment = ReplenishmentStore;
    this.auth = AuthStore;
    this.profile = ProfileStore;
    this.transfer = TransferStore;
    this.settings = SettingsStore;
    this.wallet = WalletStore;
  }
}

export default new RootStore();
