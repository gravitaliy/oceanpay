import { observable } from "mobx";
import { ReplenishmentActions } from "./replenishment.actions";

class ReplenishmentStore extends ReplenishmentActions {
  constructor(rootStore) {
    super();
    this.rootStore = rootStore;
  }

  BTC = observable({
    labels: [],
    data: [],
    dt: null,
    price: null,
    monthChangeP: null,
  });

  ETH = observable({
    labels: [],
    data: [],
    dt: null,
    price: null,
    monthChangeP: null,
  });

  USDT = observable({
    labels: [],
    data: [],
    dt: null,
    price: null,
    monthChangeP: null,
  });
}

export default new ReplenishmentStore();
