import axios from "axios";
import moment from "moment";

const api = "https://min-api.cryptocompare.com/data";
const currencies = ["BTC", "ETH", "USDT"];
const tsym = "USD";

export class ReplenishmentActions {
  getHistory(params) {
    return axios.get(`${api}/v2/histoday`, { params });
  }

  getCurrentPrices(params) {
    return axios.get(`${api}/pricemulti`, { params });
  }

  setValue(type, name, value) {
    this[type][name] = value;
  }

  setChangePrice(price, currency) {
    const changeP = ((price - this[currency].dt) / this[currency].dt) * 100;

    this.setValue(currency, "price", price.toFixed(2));
    this.setValue(currency, "monthChangeP", changeP.toFixed(2));
  }

  handleGetHistoriesAndPrice() {
    const promisesData = (data) =>
      [...data].map((item) => {
        return item.promise();
      });
    const dataHistory = currencies.map((item) => ({
      type: "history",
      currency: item,
      promise: async () =>
        this.getHistory({
          fsym: item,
          tsym,
        }),
    }));
    const dataPrices = {
      type: "prices",
      promise: async () =>
        this.getCurrentPrices({
          fsyms: currencies.join(),
          tsyms: tsym,
        }),
    };
    const data = [...dataHistory, dataPrices];

    Promise.all(promisesData(data)).then((res) => {
      data.forEach((dataItems, dataId) => {
        const resDataId = res[dataId].data;

        /* HISTORY */

        if (dataItems.type === "history") {
          const sortedData = resDataId.Data.Data.map((item, i) => {
            return {
              d: moment.unix(item.time).format("MMM DD"),
              p: item.high,
              x: i,
              y: item.high,
            };
          });

          const dsD = sortedData.map((item) => item.p);
          const testDateObj = sortedData.map((item) => item.d);

          this.setValue(dataItems.currency, "labels", testDateObj);
          this.setValue(dataItems.currency, "data", dsD);
          this.setValue(dataItems.currency, "dt", sortedData[0].y);
        }

        /* PRICES */

        if (dataItems.type === "prices") {
          currencies.forEach((item) => {
            this.setChangePrice(resDataId[item][tsym], item);
          });
        }
      });
    });
  }
}
