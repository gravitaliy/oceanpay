import { toast } from "react-toastify";
import { axiosInstance } from "../../api/axios";

export class WalletAction {
  setBalance(name, value) {
    this.coins[name].balance = value;
  }

  setAddresses(name, value) {
    this.coins[name].addresses = value;
  }

  setLoading(state) {
    this.loading = state;
  }

  setLoadingModal(state) {
    this.loadingModal = state;
  }

  setLoadingNewAddress(state) {
    this.loadingNewAddress = state;
  }

  setTransfers(value) {
    this.transfers = value;
  }

  setActiveTransfer(value) {
    this.activeTransfer = value;
  }

  setActiveAddress(value) {
    this.activeAddress = value;
  }

  promisesData = (data) => [...data].map((item) => item.promise());


  /* ================== GET BALANCE ================== */

  getBalance(currences) {
    this.setLoading(true);

    const currencesWithPromise = currences.map(item => {
      return {
        ...item,
        promise: async () => axiosInstance.get("/v1/balance/coin", { params: { coin: item.value } })
      };
    });

    Promise.all(this.promisesData(currencesWithPromise))
      .then(res => {
        const reducer = (accumulator, currentValue) => accumulator + currentValue;

        res.forEach(({ data }, idx) => {
          const { value } = currences[idx];

          this.setAddresses(value, data.data);

          if (Object.values(data.data).length !== 0) {
            const balancesArray = [...Object.values(data.data)].map(item => item.confirmed);
            const balance = balancesArray.reduce(reducer);

            this.setBalance(value, balance);
          }
        });

        this.setLoading(false);
      })
      .catch(() => {
        this.setLoading(false);
      })
  }


  /* ================== POST ADDRESS ADD ================== */

  postAddressAdd(coin) {
    this.setLoadingNewAddress(true);

    axiosInstance
      .post("/v1/address/add", { coin })
      .then(({ data }) => {
        const newAddress = {
          [data.data.address]: {
            confirmed: 0,
            unconfirmed: 0,
          }
        };
        this.setAddresses(coin, ({ ...this.coins[coin].addresses, ...newAddress}));
        this.setLoadingNewAddress(false);
        toast.success("Address has been successfully added");
      })
      .catch(err => {
        this.setLoadingNewAddress(false);
        toast.error(err?.response?.data?.data?.message);
      });
  }


  /* ================== GET TXS LIST ================== */

  getTxsList(coin) {
    axiosInstance
      .get("/v1/tx/list", { params: { coin } })
      .then(({ data }) => {
        this.setTransfers(data.data.txs.transfers);
      })
      .catch(err => {
        toast.error(err?.response?.data?.data?.message);
      });
  }


  /* ================== GET TX INFO ================== */

  getTxInfo(coin, id) {
    this.setLoadingModal(true);

    axiosInstance
      .get("/v1/tx", { params: { coin, id } })
      .then(({ data }) => {
        this.setActiveTransfer(data.data.tx);
        this.setLoadingModal(false);
      })
      .catch(err => {
        this.setLoadingModal(false);
        toast.error(err?.response?.data?.data?.message);
      });
  }


  /* ================== GET ADDRESS ================== */

  getAddress(coin, id) {
    // this.setLoadingModal(true);

    this.setActiveAddress({ address: id });

/*
    axiosInstance
      .get("/v1/address", { params: { coin, id } })
      .then(({ data }) => {
        this.setActiveAddress(data.data);
        this.setLoadingModal(false);
      })
      .catch(err => {
        this.setLoadingModal(false);
        toast.error(err?.response?.data?.data?.message);
      });
*/
  }
}
