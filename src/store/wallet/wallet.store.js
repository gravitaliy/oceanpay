import { decorate, observable } from "mobx";
import "mobx-react-lite/batchingForReactDom";

// local files
import { WalletAction } from "./wallet.action";

class WalletStore extends WalletAction {
  coins = {
    tbtc: {
      balance: 0,
      addresses: {},
    },
    teth: {
      balance: 0,
      addresses: {},
    },
    tbch: {
      balance: 0,
      addresses: {},
    },
    tltc: {
      balance: 0,
      addresses: {},
    },
    cron: {
      balance: 0,
      addresses: {},
    },
    rudt: {
      balance: 0,
      addresses: {},
    },
  };

  loading = true;

  loadingModal = false;

  loadingNewAddress = false;

  transfers = [];

  activeTransfer = {};

  activeAddress = {};
}

decorate(WalletStore, {
  coins: observable,
  loading: observable,
  loadingModal: observable,
  loadingNewAddress: observable,
  transfers: observable,
  activeTransfer: observable,
  activeAddress: observable,
});

export default new WalletStore();
