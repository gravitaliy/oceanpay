import { toast } from "react-toastify";
import { axiosInstanceAuth } from "../../api/axios";
import { TOKEN } from "../../assets/constans/auth.const";
import {
  clearData,
  getDataWithoutPasswordConfirmation,
} from "../../assets/functions";
import { mock } from "../../assets/mock/mock";

export class AuthAction {
  setLoading(state) {
    this.loading = state;
  }

  setValue(name, value) {
    this[name] = value;
  }

  setRestorePassword(name, value) {
    this.resetPassword[name] = value;
  }

  /*
  async sendSignIn() {
    this.setLoading(true);
    try {
      const response = await axiosInstanceAuth.post(
        "/Auth/SignIn",
        this.signIn,
      );
      this.setLoading(false);
      if (response?.data?.token) {
        const { token } = response?.data;
        localStorage.setItem(TOKEN, token);
        this.setToken(token);
        return token;
      }
      return false;
    } catch (err) {
      this.setLoading(false);
      toast.error(err?.response?.data?.message);
      return false;
    }
  }
*/

  sendSignIn(redirect) {
    this.setLoading(true);
    // console.log(2, "axiosInstanceAuth", axiosInstanceAuth.defaults)

    setTimeout(() => {
      const response = {
        data: {
          token: "token",
        }
      }

      this.setLoading(false);
      if (response?.data?.token) {
        const { token } = response?.data;
        localStorage.setItem(TOKEN, token);
        this.setToken(token);
      }
      redirect();
    }, 300);

/*
    axiosInstanceAuth
      .post("/Auth/SignIn", this.signIn)
      .then(response => {
        this.setLoading(false);
        if (response?.data?.token) {
          const { token } = response?.data;
          localStorage.setItem(TOKEN, token);
          this.setToken(token);
          redirect();
        }
      })
      .catch(err => {
        this.setLoading(false);
        toast.error(err?.response?.data?.message);
      });
*/
  }

  sendSignUp(redirect) {
    this.setLoading(true);

    axiosInstanceAuth
      .post("/Auth/SignUp", getDataWithoutPasswordConfirmation(this.signUp))
      .then(response => {
        this.setValue("signUp", clearData(this.signUp));
        redirect();
        this.setLoading(false);
        toast.warning(response?.data?.status);
      })
      .catch(err => {
        this.setLoading(false);
        toast.error(err?.response?.data?.message);
      });
  }

  sendRequestPassword() {
    this.setLoading(true);

    axiosInstanceAuth
      .post("/Auth/RestorePassword", this.requestPassword)
      .then(() => {
        this.setLoading(false);
        toast.success("Success! Сheck your e-mail");
      })
      .catch(err => {
        this.setLoading(false);
        toast.error(err?.response?.data?.message);
      });
  }

  sendResendVerificationMail() {
    this.setLoading(true);

    axiosInstanceAuth
      .post("/Auth/ResendVerificationMail", this.requestPassword)
      .then(() => {
        this.setLoading(false);
        toast.success("Success! Сheck your e-mail");
      })
      .catch(err => {
        this.setLoading(false);
        toast.error(err?.response?.data?.message);
      });
  }

  sendRestorePassword(redirect) {
    this.setLoading(true);

    axiosInstanceAuth
      .post("/Auth/RestorePasswordSet", this.resetPassword)
      .then(() => {
        redirect();
        this.setLoading(false);
        toast.success("The password has been successfully changed");
      })
      .catch(err => {
        this.setLoading(false);
        toast.error(err?.response?.data?.message);
      });
  }

  getUserInfo() {
    setTimeout(() => {
      this.setValue("info", {
        email: mock.user_info.email,
        login: mock.user_info.login,
      });
    }, 300);
/*
    axiosInstanceAuth
      .post("/Auth/GetUserInfo", { token: this.token })
      .then(response => {
        this.setValue("info", {
          email: response.data.user_info.email,
          login: response.data.user_info.login,
        });
      })
      .catch(err => {
        toast.error(err?.response?.data?.message);
      });
*/
  }
}
