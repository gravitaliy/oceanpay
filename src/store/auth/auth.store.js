import { decorate, observable } from "mobx";
import "mobx-react-lite/batchingForReactDom";

// local files
import { AuthAction } from "./auth.action";
import { TOKEN } from "../../assets/constans/auth.const";
import { idPasswordConfirmation } from "../../assets/functions";

class AuthStore extends AuthAction {
  token = localStorage.getItem(TOKEN);

  setToken(token) {
    this.token = token;
    localStorage.setItem(TOKEN, token);
  }

  loading = false;

  signIn = {
    email: "email@email.com",
    password: "111",
  };

  signUp = {
    login: "",
    email: "",
    password: "",
    [idPasswordConfirmation]: "",
  };

  requestPassword = {
    email: "",
  };

  resetPassword = {
    email: "",
    restore_code: "",
    new_password: "",
  };

  info = {};
}

decorate(AuthStore, {
  token: observable,
  loading: observable,
  signIn: observable,
  signUp: observable,
  requestPassword: observable,
  resetPassword: observable,
  info: observable,
});

export default new AuthStore();
