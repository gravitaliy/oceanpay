import { decorate, observable } from "mobx";
import "mobx-react-lite/batchingForReactDom";

// local files
import { DashboardAction } from "./dashboard.action";

class DashboardStore extends DashboardAction {
  modalName = null;

  loading = true;

  cardInfo = null;

  cardsList = null;

  transactions = null;
}

decorate(DashboardStore, {
  modalName: observable,
  loading: observable,
  cardInfo: observable,
  cardsList: observable,
  transactions: observable,
});

export default new DashboardStore();
