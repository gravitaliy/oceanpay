import { toast } from "react-toastify";
import { axiosInstance } from "../../api/axios";
import { mock } from "../../assets/mock/mock";

export class DashboardAction {
  updateModalName(modalName) {
    this.modalName = modalName;
  }

  setLoading(state) {
    this.loading = state;
  }

  getCardsListInfo = (cardNumber) => {
    this.setLoading(true);

    const body = {
      card_info_type: "FULL_CARD_INFO",
      card_number: cardNumber,
    };

    setTimeout(() => {
      this.cardsList = mock.cardsList;
    }, 300);

/*
    axiosInstance.post("/Cryptocard/GetCardInfo", body).then(
      (res) => {
        this.setLoading(false);
        if (res?.data?.list_of_full_card_info) {
          this.cardsList = res.data.list_of_full_card_info;
        }
      },
      () => {
        this.setLoading(false);
        // console.log(err?.response?.data?.message);
        /!*
        if (err?.response?.data?.message) {
          toast.error(err?.response?.data?.message);
        }
*!/
      },
    );
*/
  };

  getTransactionsList = (cardNumber) => {
    // this.setLoading(true);

    const body = {
      card_number: cardNumber,
      limit: 100,
      offset: 0,
    };

    setTimeout(() => {
      this.transactions = mock.transactions;
      this.setLoading(false);
    }, 300);

/*
    axiosInstance.post("/Cryptocard/GetTransactions", body).then(
      (res) => {
        if (res?.status === 200 && res?.data) {
          this.transactions = res.data.list_of_transactions;
          // this.setLoading(false);
        }
      },
      (err) => {
        this.setLoading(false);
        if (err?.response?.data?.message) {
          toast.error(err?.response?.data?.message);
        }
      },
    );
*/
  };

  bindCard(cardData) {
    this.setLoading(true);

    axiosInstance.post("/Cryptocard/Bind", { card_number: cardData }).then(
      (res) => {
        if (res?.status === 200 && res?.data) {
          this.cardInfo = res.data;
          this.getCardsListInfo();
        }

        this.setLoading(false);
        toast.success("Сard has been successfully binded");
      },
      (err) => {
        this.setLoading(false);
        if (err?.response?.data?.message) {
          toast.error(err?.response?.data?.message);
        }
      },
    );
  }

  unbindCard(cardData) {
    this.setLoading(true);

    setTimeout(() => {
      this.cardsList = [];
      this.setLoading(false);
      toast.success("Сard has been successfully unbinded");
    }, 300);

/*
    axiosInstance.post("/Cryptocard/Unbind", { card_number: cardData }).then(
      () => {
        this.setLoading(false);
        toast.success("Сard has been successfully unbinded");
      },
      (err) => {
        this.setLoading(false);
        if (err?.response?.data?.message) {
          toast.error(err?.response?.data?.message);
        }
      },
    );
*/
  }
}
