import { decorate, observable } from "mobx";
import "mobx-react-lite/batchingForReactDom";
import { ProfileAction } from "./profile.action";
// local

class ProfileStore extends ProfileAction {
  loading = false;

  inputs = {
    cardNumber: "",
    user_info_for_update: {
      email: "",
      cell_number_country_code: "",
      cell_number: "",
      firstname: "",
      lastname: "",
      middle_name: "",
      date_of_birth: "",
      firstname_local: "",
      lastname_local: "",
      gender: "",
      nationality: "",
      occupation: "",
      address: "",
      city: "",
      postal_code: "",
      country: "",
      doc_pid_number: "",
      doc_pid_issue_at: "",
      doc_pid_expired_at: "",

      doc_pid_type: "UNKNOWN_DOC_PID_TYPE",
      doc_pid_image_url: "",
      doc_pid_image_base64: "",

      doc_pidback_type: "UNKNOWN_DOC_PID_BACK_TYPE",
      doc_pidback_image_url: "",
      doc_pidback_image_base64: "",

      doc_verification_type: "UNKNOWN_DOC_VERIFICATION_TYPE",
      doc_verification_image_url: "",
      doc_verification_image_base64: "",

      doc_sign_type: "UNKNOWN_DOC_SIGN_TYPE",
      doc_sign_image_url: "",
      doc_sign_image_base64: "",
    },
  };
}

decorate(ProfileStore, {
  loading: observable,
  inputs: observable,
});

export default new ProfileStore();
