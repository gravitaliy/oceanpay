import { toast } from "react-toastify";
import { axiosInstance } from "../../api/axios";
import { checkDate } from "../../assets/functions";
import { mock } from "../../assets/mock/mock";

export class ProfileAction {
  setLoading(state) {
    this.loading = state;
  }

  setValue(name, value) {
    this.inputs.user_info_for_update[name] = value;
  }

  setCardNumber(value) {
    this.inputs.cardNumber = value;
    if (!value) {
      Object.keys(this.inputs.user_info_for_update).forEach(item => {
        if (this.inputs.user_info_for_update[item] !== "") {
          this.setValue(item, "");
        }
      });
    }
  }

  getUserProfileInfo() {
    this.setLoading(true);

    setTimeout(() => {
      Object.entries(mock.kyc_profile_info).forEach(([key, value]) => {
        const valueSec =
          key === "cell_number_country_code"
            ? value
            : checkDate(value);
        this.setValue(key, valueSec);
      });
      this.setLoading(false);
    }, 300);
/*
    axiosInstance
      .post("/Cryptocard/GetKycInfo", {
        card_number: this.inputs.cardNumber,
      })
      .then(
        res => {
          Object.entries(res.data.kyc_profile_info).forEach(item => {
            const value =
              item[0] === "cell_number_country_code"
                ? item[1]
                : checkDate(item[1]);
            this.setValue(item[0], value);
          });
          this.setLoading(false);
        },
        err => {
          this.setLoading(false);
          if (err?.response?.data?.message) {
            toast.error(err?.response?.data?.message);
          }
        },
      );
*/
  }

  sendProfile() {
    this.setLoading(true);

    switch (this.inputs.user_info_for_update.doc_pid_type) {
      case "ID_CARD":
        this.setValue("doc_pidback_type", "ID_CARD_BACK_SIDE");
        this.setValue("doc_verification_type", "HANDHELD_ID_CARD");
        break;
      case "PASSPORT":
        this.setValue("doc_pidback_type", "ID_CARD_BACK_SIDE");
        this.setValue("doc_verification_type", "HANDHELD_PASSPORT");
    }

    axiosInstance.post("/Cryptocard/SendKycInfo", this.inputs).then(
      () => {
        this.setLoading(false);
        toast.success("The data has been successfully sent");
      },
      err => {
        this.setLoading(false);
        if (err?.response?.data?.message) {
          toast.error(err?.response?.data?.message);
        }
      },
    );
  }
}
