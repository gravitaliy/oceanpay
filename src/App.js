import React from "react";
import { Provider } from "mobx-react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

// local files
import { Dashboard, Profile, Settings, Wallets } from "./routes";
import { Header } from "./containers/Header/Header";
import { routesList } from "./assets/enums";
import store from "./store/index";
import { Auth } from "./routes/Auth";
import "./App.scss";

export const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <div className="app-wrap">
          <Header />
          <Switch>
            <Route exact path="/">
              <Redirect to={`/${routesList.AUTH.base}`} />
            </Route>
            <Route
              path={`/${routesList.AUTH.base}`}
              render={() => {
                return store.auth.token ? (
                  <Redirect to={routesList.HOME} />
                ) : (
                  <Auth />
                );
              }}
            />
            <Route
              path={routesList.HOME}
              title="Dashboard"
              render={() => {
                return !store.auth.token ? <Redirect to="/" /> : <Dashboard />;
              }}
            />
            <Route
              path={routesList.PROFILE}
              title="Profile"
              render={() => {
                return !store.auth.token ? <Redirect to="/" /> : <Profile />;
              }}
            />
            <Route
              path={routesList.SETTINGS}
              title="Setting"
              render={() => {
                return !store.auth.token ? <Redirect to="/" /> : <Settings />;
              }}
            />
            <Route
              path={routesList.WALLETS}
              title="Wallets"
              render={() => {
                return !store.auth.token ? <Redirect to="/" /> : <Wallets />;
              }}
            />
            <Route path="*">
              <div className="h1">404 page not found</div>
            </Route>
          </Switch>
          <ToastContainer
            position="bottom-right"
            autoClose={5000}
            hideProgressBar
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
          />
        </div>
      </Router>
    </Provider>
  );
};
