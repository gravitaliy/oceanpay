export const mock = {
  user_info: {
    email: "email@email.com",
    login: "login",
  },

  cardsList: [
    {
      card_permission: {
        balance_limit: 10,
      },
      card_info: {
        kyc_status: "APPROVED",
        balance: "10",
        number: "4276550066667777",
        pin: "786525",
        currency: "USD",
      },
      card_crypto_addresses: {
        cryptocurrencyAcronymAddressPair: {
          btc: "iq2438y98jf439rf",
          eth: "io9823u3jrf8938r9",
          usdteth: "wfioeru9234r89",
          usdtomni: "2390rekfrjg98",
        },
        cryptocurrencyAcronymRatePair: {
          btc: 48438.38,
          eth: 1563.11,
          usdteth: 1.001,
        },
      },
    }
  ],

  transactions: [
    {
      amount: 10,
      merchant: "merchant1",
      executed_at: "2022.11.20 14:55",
      currency: "USD",
      reference: 101,
      transaction_status: "SUCC"
    },
    {
      amount: -11,
      merchant: "merchant2",
      executed_at: "2022.12.20 12:32",
      currency: "USD",
      reference: 102,
      transaction_status: "FAIL"
    },
    {
      amount: 22,
      merchant: "merchant3",
      executed_at: "2022.12.10 13:03",
      currency: "USD",
      reference: 103,
      transaction_status: "AUTH"
    },
  ],

  kyc_profile_info: {
    email: "email@email.com",
    cell_number_country_code: "+7",
    cell_number: "9554656565",
    firstname: "firstname",
    lastname: "lastname",
    middle_name: "middle_name",
    date_of_birth: "11.02.1990",
    firstname_local: "Имя",
    lastname_local: "Фамилия",
    gender: "M",
    nationality: "RUS",
    occupation: "Doctor__Assistant_Medical_Staff",
    address: "address",
    city: "city",
    postal_code: "postal_code",
    country: "RUS",
    doc_pid_number: "doc_pid_number",
    doc_pid_issue_at: "doc_pid_issue_at",
    doc_pid_expired_at: "doc_pid_expired_at",

    doc_pid_type: "ID_CARD",
    doc_pid_image_url: "https://www.plant.ca/wp-content/uploads/2018/09/test-img-300x194.png",
    doc_pid_image_base64: "",

    doc_pidback_type: "ID_CARD",
    doc_pidback_image_url: "",
    doc_pidback_image_base64: "",

    doc_verification_type: "UNKNOWN_DOC_VERIFICATION_TYPE",
    doc_verification_image_url: "",
    doc_verification_image_base64: "",

    doc_sign_type: "UNKNOWN_DOC_SIGN_TYPE",
    doc_sign_image_url: "",
    doc_sign_image_base64: "",
  },
}
