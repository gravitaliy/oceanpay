export const countryList = [
  {
    label: "Afghanistan",
    value: "AFG",
    id: 1,
  },
  {
    label: "Albania",
    value: "ALB",
    id: 2,
  },
  {
    label: "Algeria",
    value: "DZA",
    id: 3,
  },
  {
    label: "American Samoa",
    value: "ASM",
    id: 4,
  },
  {
    label: "Andorra",
    value: "AND",
    id: 5,
  },
  {
    label: "Angola",
    value: "AGO",
    id: 6,
  },
  {
    label: "Anguilla",
    value: "AIA",
    id: 7,
  },
  {
    label: "Antarctica",
    value: "ATA",
    id: 8,
  },
  {
    label: "Antigua and Barbuda",
    value: "ATG",
    id: 9,
  },
  {
    label: "Argentina",
    value: "ARG",
    id: 10,
  },
  {
    label: "Armenia",
    value: "ARM",
    id: 11,
  },
  {
    label: "Aruba",
    value: "ABW",
    id: 12,
  },
  {
    label: "Australia",
    value: "AUS",
    id: 13,
  },
  {
    label: "Austria",
    value: "AUT",
    id: 14,
  },
  {
    label: "Azerbaijan",
    value: "AZE",
    id: 15,
  },
  {
    label: "Bahamas",
    value: "BHS",
    id: 16,
  },
  {
    label: "Bahrain",
    value: "BHR",
    id: 17,
  },
  {
    label: "Bangladesh",
    value: "BGD",
    id: 18,
  },
  {
    label: "Barbados",
    value: "BRB",
    id: 19,
  },
  {
    label: "Belarus",
    value: "BLR",
    id: 20,
  },
  {
    label: "Belgium",
    value: "BEL",
    id: 21,
  },
  {
    label: "Belize",
    value: "BLZ",
    id: 22,
  },
  {
    label: "Benin",
    value: "BEN",
    id: 23,
  },
  {
    label: "Bermuda",
    value: "BMU",
    id: 24,
  },
  {
    label: "Bhutan",
    value: "BTN",
    id: 25,
  },
  {
    label: "Bolivia (Plurinational State of)",
    value: "BOL",
    id: 26,
  },
  {
    label: "Bonaire, Sint Eustatius and Saba",
    value: "BES",
    id: 27,
  },
  {
    label: "Bosnia and Herzegovina",
    value: "BIH",
    id: 28,
  },
  {
    label: "Botswana",
    value: "BWA",
    id: 29,
  },
  {
    label: "Bouvet Island",
    value: "BVT",
    id: 30,
  },
  {
    label: "Brazil",
    value: "BRA",
    id: 31,
  },
  {
    label: "British Indian Ocean Territory",
    value: "IOT",
    id: 32,
  },
  {
    label: "Brunei Darussalam",
    value: "BRN",
    id: 33,
  },
  {
    label: "Bulgaria",
    value: "BGR",
    id: 34,
  },
  {
    label: "Burkina Faso",
    value: "BFA",
    id: 35,
  },
  {
    label: "Burundi",
    value: "BDI",
    id: 36,
  },
  {
    label: "Cabo Verde",
    value: "CPV",
    id: 37,
  },
  {
    label: "Cambodia",
    value: "KHM",
    id: 38,
  },
  {
    label: "Cameroon",
    value: "CMR",
    id: 39,
  },
  {
    label: "Canada",
    value: "CAN",
    id: 40,
  },
  {
    label: "Cayman Islands",
    value: "CYM",
    id: 41,
  },
  {
    label: "Central African Republic",
    value: "CAF",
    id: 42,
  },
  {
    label: "Chad",
    value: "TCD",
    id: 43,
  },
  {
    label: "Chile",
    value: "CHL",
    id: 44,
  },
  {
    label: "China",
    value: "CHN",
    id: 45,
  },
  {
    label: "Christmas Island",
    value: "CXR",
    id: 46,
  },
  {
    label: "Cocos (Keeling) Islands",
    value: "CCK",
    id: 47,
  },
  {
    label: "Colombia",
    value: "COL",
    id: 48,
  },
  {
    label: "Comoros",
    value: "COM",
    id: 49,
  },
  {
    label: "Congo",
    value: "COG",
    id: 50,
  },
  {
    label: "Congo (Democratic Republic of the)",
    value: "COD",
    id: 51,
  },
  {
    label: "Cook Islands",
    value: "COK",
    id: 52,
  },
  {
    label: "Costa Rica",
    value: "CRI",
    id: 53,
  },
  {
    label: "Croatia",
    value: "HRV",
    id: 54,
  },
  {
    label: "Cuba",
    value: "CUB",
    id: 55,
  },
  {
    label: "Curaçao",
    value: "CUW",
    id: 56,
  },
  {
    label: "Cyprus",
    value: "CYP",
    id: 57,
  },
  {
    label: "Czech Republic",
    value: "CZE",
    id: 58,
  },
  {
    label: "Côte d'Ivoire",
    value: "CIV",
    id: 59,
  },
  {
    label: "Denmark",
    value: "DNK",
    id: 60,
  },
  {
    label: "Djibouti",
    value: "DJI",
    id: 61,
  },
  {
    label: "Dominica",
    value: "DMA",
    id: 62,
  },
  {
    label: "Dominican Republic",
    value: "DOM",
    id: 63,
  },
  {
    label: "Ecuador",
    value: "ECU",
    id: 64,
  },
  {
    label: "Egypt",
    value: "EGY",
    id: 65,
  },
  {
    label: "El Salvador",
    value: "SLV",
    id: 66,
  },
  {
    label: "Equatorial Guinea",
    value: "GNQ",
    id: 67,
  },
  {
    label: "Eritrea",
    value: "ERI",
    id: 68,
  },
  {
    label: "Estonia",
    value: "EST",
    id: 69,
  },
  {
    label: "Ethiopia",
    value: "ETH",
    id: 70,
  },
  {
    label: "Falkland Islands (Malvinas)",
    value: "FLK",
    id: 71,
  },
  {
    label: "Faroe Islands",
    value: "FRO",
    id: 72,
  },
  {
    label: "Fiji",
    value: "FJI",
    id: 73,
  },
  {
    label: "Finland",
    value: "FIN",
    id: 74,
  },
  {
    label: "France",
    value: "FRA",
    id: 75,
  },
  {
    label: "French Guiana",
    value: "GUF",
    id: 76,
  },
  {
    label: "French Polynesia",
    value: "PYF",
    id: 77,
  },
  {
    label: "French Southern Territories",
    value: "ATF",
    id: 78,
  },
  {
    label: "Gabon",
    value: "GAB",
    id: 79,
  },
  {
    label: "Gambia",
    value: "GMB",
    id: 80,
  },
  {
    label: "Georgia",
    value: "GEO",
    id: 81,
  },
  {
    label: "Germany",
    value: "DEU",
    id: 82,
  },
  {
    label: "Ghana",
    value: "GHA",
    id: 83,
  },
  {
    label: "Gibraltar",
    value: "GIB",
    id: 84,
  },
  {
    label: "Greece",
    value: "GRC",
    id: 85,
  },
  {
    label: "Greenland",
    value: "GRL",
    id: 86,
  },
  {
    label: "Grenada",
    value: "GRD",
    id: 87,
  },
  {
    label: "Guadeloupe",
    value: "GLP",
    id: 88,
  },
  {
    label: "Guam",
    value: "GUM",
    id: 89,
  },
  {
    label: "Guatemala",
    value: "GTM",
    id: 90,
  },
  {
    label: "Guernsey",
    value: "GGY",
    id: 91,
  },
  {
    label: "Guinea",
    value: "GIN",
    id: 92,
  },
  {
    label: "Guinea-Bissa",
    value: "GNB",
    id: 93,
  },
  {
    label: "Guyana",
    value: "GUY",
    id: 94,
  },
  {
    label: "Haiti",
    value: "HTI",
    id: 95,
  },
  {
    label: "Heard Island and McDonald Islands",
    value: "HMD",
    id: 96,
  },
  {
    label: "Holy See",
    value: "VAT",
    id: 97,
  },
  {
    label: "Honduras",
    value: "HND",
    id: 98,
  },
  {
    label: "Hong Kong",
    value: "HKG",
    id: 99,
  },
  {
    label: "Hungary",
    value: "HUN",
    id: 100,
  },
  {
    label: "Iceland",
    value: "ISL",
    id: 101,
  },
  {
    label: "India",
    value: "IND",
    id: 102,
  },
  {
    label: "Indonesia",
    value: "IDN",
    id: 103,
  },
  {
    label: "Iran (Islamic Republic of)",
    value: "IRN",
    id: 104,
  },
  {
    label: "Iraq",
    value: "IRQ",
    id: 105,
  },
  {
    label: "Ireland",
    value: "IRL",
    id: 106,
  },
  {
    label: "Isle of Man",
    value: "IMN",
    id: 107,
  },
  {
    label: "Israel",
    value: "ISR",
    id: 108,
  },
  {
    label: "Italy",
    value: "ITA",
    id: 109,
  },
  {
    label: "Jamaica",
    value: "JAM",
    id: 110,
  },
  {
    label: "Japan",
    value: "JPN",
    id: 111,
  },
  {
    label: "Jersey",
    value: "JEY",
    id: 112,
  },
  {
    label: "Jordan",
    value: "JOR",
    id: 113,
  },
  {
    label: "Kazakhstan",
    value: "KAZ",
    id: 114,
  },
  {
    label: "Kenya",
    value: "KEN",
    id: 115,
  },
  {
    label: "Kiribati",
    value: "KIR",
    id: 116,
  },
  {
    label: "Korea (Democratic People's Republic of)",
    value: "PRK",
    id: 117,
  },
  {
    label: "Korea (Republic of)",
    value: "KOR",
    id: 118,
  },
  {
    label: "Kuwait",
    value: "KWT",
    id: 119,
  },
  {
    label: "Kyrgyzstan",
    value: "KGZ",
    id: 120,
  },
  {
    label: "Lao People's Democratic Republic",
    value: "LAO",
    id: 121,
  },
  {
    label: "Latvia",
    value: "LVA",
    id: 122,
  },
  {
    label: "Lebanon",
    value: "LBN",
    id: 123,
  },
  {
    label: "Lesotho",
    value: "LSO",
    id: 124,
  },
  {
    label: "Liberia",
    value: "LBR",
    id: 125,
  },
  {
    label: "Libya",
    value: "LBY",
    id: 126,
  },
  {
    label: "Liechtenstein",
    value: "LIE",
    id: 127,
  },
  {
    label: "Lithuania",
    value: "LTU",
    id: 128,
  },
  {
    label: "Luxembourg",
    value: "LUX",
    id: 129,
  },
  {
    label: "Macao",
    value: "MAC",
    id: 130,
  },
  {
    label: "Macedonia (the former Yugoslav Republic of)",
    value: "MKD",
    id: 131,
  },
  {
    label: "Madagascar",
    value: "MDG",
    id: 132,
  },
  {
    label: "Malawi",
    value: "MWI",
    id: 133,
  },
  {
    label: "Malaysia",
    value: "MYS",
    id: 134,
  },
  {
    label: "Maldives",
    value: "MDV",
    id: 135,
  },
  {
    label: "Mali",
    value: "MLI",
    id: 136,
  },
  {
    label: "Malta",
    value: "MLT",
    id: 137,
  },
  {
    label: "Marshall Islands",
    value: "MHL",
    id: 138,
  },
  {
    label: "Martinique",
    value: "MTQ",
    id: 139,
  },
  {
    label: "Mauritania",
    value: "MRT",
    id: 140,
  },
  {
    label: "Mauritius",
    value: "MUS",
    id: 141,
  },
  {
    label: "Mayotte",
    value: "MYT",
    id: 142,
  },
  {
    label: "Mexico",
    value: "MEX",
    id: 143,
  },
  {
    label: "Micronesia (Federated States of)",
    value: "FSM",
    id: 144,
  },
  {
    label: "Moldova (Republic of)",
    value: "MDA",
    id: 145,
  },
  {
    label: "Monaco",
    value: "MCO",
    id: 146,
  },
  {
    label: "Mongolia",
    value: "MNG",
    id: 147,
  },
  {
    label: "Montenegro",
    value: "MNE",
    id: 148,
  },
  {
    label: "Montserrat",
    value: "MSR",
    id: 149,
  },
  {
    label: "Morocco",
    value: "MAR",
    id: 150,
  },
  {
    label: "Mozambique",
    value: "MOZ",
    id: 151,
  },
  {
    label: "Myanmar",
    value: "MMR",
    id: 152,
  },
  {
    label: "Namibia",
    value: "NAM",
    id: 153,
  },
  {
    label: "Nauru",
    value: "NRU",
    id: 154,
  },
  {
    label: "Nepal",
    value: "NPL",
    id: 155,
  },
  {
    label: "Netherlands",
    value: "NLD",
    id: 156,
  },
  {
    label: "New Caledonia",
    value: "NCL",
    id: 157,
  },
  {
    label: "New Zealand",
    value: "NZL",
    id: 158,
  },
  {
    label: "Nicaragua",
    value: "NIC",
    id: 159,
  },
  {
    label: "Niger",
    value: "NER",
    id: 160,
  },
  {
    label: "Nigeria",
    value: "NGA",
    id: 161,
  },
  {
    label: "Niue",
    value: "NIU",
    id: 162,
  },
  {
    label: "Norfolk Island",
    value: "NFK",
    id: 163,
  },
  {
    label: "Northern Mariana Islands",
    value: "MNP",
    id: 164,
  },
  {
    label: "Norway",
    value: "NOR",
    id: 165,
  },
  {
    label: "Oman",
    value: "OMN",
    id: 166,
  },
  {
    label: "Pakistan",
    value: "PAK",
    id: 167,
  },
  {
    label: "Palau",
    value: "PLW",
    id: 168,
  },
  {
    label: "Palestine, State of",
    value: "PSE",
    id: 169,
  },
  {
    label: "Panama",
    value: "PAN",
    id: 170,
  },
  {
    label: "Papua New Guinea",
    value: "PNG",
    id: 171,
  },
  {
    label: "Paraguay",
    value: "PRY",
    id: 172,
  },
  {
    label: "Peru",
    value: "PER",
    id: 173,
  },
  {
    label: "Philippines",
    value: "PHL",
    id: 174,
  },
  {
    label: "Pitcairn",
    value: "PCN",
    id: 175,
  },
  {
    label: "Poland",
    value: "POL",
    id: 176,
  },
  {
    label: "Portugal",
    value: "PRT",
    id: 177,
  },
  {
    label: "Puerto Rico",
    value: "PRI",
    id: 178,
  },
  {
    label: "Qatar",
    value: "QAT",
    id: 179,
  },
  {
    label: "Romania",
    value: "ROU",
    id: 180,
  },
  {
    label: "Russian Federation",
    value: "RUS",
    id: 181,
  },
  {
    label: "Rwanda",
    value: "RWA",
    id: 182,
  },
  {
    label: "Réunion",
    value: "REU",
    id: 183,
  },
  {
    label: "Saint Barthélemy",
    value: "BLM",
    id: 184,
  },
  {
    label: "Saint Helena, Ascension and Tristan da Cunha",
    value: "SHN",
    id: 185,
  },
  {
    label: "Saint Kitts and Nevis",
    value: "KNA",
    id: 186,
  },
  {
    label: "Saint Lucia",
    value: "LCA",
    id: 187,
  },
  {
    label: "Saint Martin (French part)",
    value: "MAF",
    id: 188,
  },
  {
    label: "Saint Pierre and Miquelon",
    value: "SPM",
    id: 189,
  },
  {
    label: "Saint Vincent and the Grenadines",
    value: "VCT",
    id: 190,
  },
  {
    label: "Samoa",
    value: "WSM",
    id: 191,
  },
  {
    label: "San Marino",
    value: "SMR",
    id: 192,
  },
  {
    label: "Sao Tome and Principe",
    value: "STP",
    id: 193,
  },
  {
    label: "Saudi Arabia",
    value: "SAU",
    id: 194,
  },
  {
    label: "Senegal",
    value: "SEN",
    id: 195,
  },
  {
    label: "Serbia",
    value: "SRB",
    id: 196,
  },
  {
    label: "Seychelles",
    value: "SYC",
    id: 197,
  },
  {
    label: "Sierra Leone",
    value: "SLE",
    id: 198,
  },
  {
    label: "Singapore",
    value: "SGP",
    id: 199,
  },
  {
    label: "Sint Maarten (Dutch part)",
    value: "SXM",
    id: 200,
  },
  {
    label: "// Slovaki",
    value: "SVK",
    id: 201,
  },
  {
    label: "Slovenia",
    value: "SVN",
    id: 202,
  },
  {
    label: "Solomon Islands",
    value: "SLB",
    id: 203,
  },
  {
    label: "Somalia",
    value: "SOM",
    id: 204,
  },
  {
    label: "South Africa",
    value: "ZAF",
    id: 205,
  },
  {
    label: "South Georgia and the South Sandwich Islands",
    value: "SGS",
    id: 206,
  },
  {
    label: "South Sudan",
    value: "SSD",
    id: 207,
  },
  {
    label: "Spain",
    value: "ESP",
    id: 208,
  },
  {
    label: "Sri Lanka",
    value: "LKA",
    id: 209,
  },
  {
    label: "Sudan",
    value: "SDN",
    id: 210,
  },
  {
    label: "Suriname",
    value: "SUR",
    id: 211,
  },
  {
    label: "Svalbard and Jan Mayen",
    value: "SJM",
    id: 212,
  },
  {
    label: "Swaziland",
    value: "SWZ",
    id: 213,
  },
  {
    label: "Sweden",
    value: "SWE",
    id: 214,
  },
  {
    label: "Switzerland",
    value: "CHE",
    id: 215,
  },
  {
    label: "Syrian Arab Republic",
    value: "SYR",
    id: 216,
  },
  {
    label: "Taiwan, Province of China",
    value: "TWN",
    id: 217,
  },
  {
    label: "Tajikistan",
    value: "TJK",
    id: 218,
  },
  {
    label: "Tanzania, United Republic of",
    value: "TZA",
    id: 219,
  },
  {
    label: "Thailand",
    value: "THA",
    id: 220,
  },
  {
    label: "Timor-Lest",
    value: "TLS",
    id: 221,
  },
  {
    label: "Togo",
    value: "TGO",
    id: 222,
  },
  {
    label: "Tokelau",
    value: "TKL",
    id: 223,
  },
  {
    label: "Tonga",
    value: "TON",
    id: 224,
  },
  {
    label: "Trinidad and Tobago",
    value: "TTO",
    id: 225,
  },
  {
    label: "Tunisia",
    value: "TUN",
    id: 226,
  },
  {
    label: "Turkey",
    value: "TUR",
    id: 227,
  },
  {
    label: "Turkmenistan",
    value: "TKM",
    id: 228,
  },
  {
    label: "Turks and Caicos Islands",
    value: "TCA",
    id: 229,
  },
  {
    label: "Tuvalu",
    value: "TUV",
    id: 230,
  },
  {
    label: "Uganda",
    value: "UGA",
    id: 240,
  },
  {
    label: "Ukraine",
    value: "UKR",
    id: 241,
  },
  {
    label: "United Arab Emirates",
    value: "ARE",
    id: 242,
  },
  {
    label: "United Kingdom of Great Britain and Northern Ireland",
    value: "GBR",
    id: 243,
  },
  {
    label: "United States Minor Outlying Islands",
    value: "UMI",
    id: 244,
  },
  {
    label: "United States of America",
    value: "USA",
    id: 245,
  },
  {
    label: "Uruguay",
    value: "URY",
    id: 246,
  },
  {
    label: "Uzbekistan",
    value: "UZB",
    id: 247,
  },
  {
    label: "Vanuatu",
    value: "VUT",
    id: 248,
  },
  {
    label: "Venezuela (Bolivarian Republic of)",
    value: "VEN",
    id: 249,
  },
  {
    label: "Viet Nam",
    value: "VNM",
    id: 250,
  },
  {
    label: "Virgin Islands (British)",
    value: "VGB",
    id: 251,
  },
  {
    label: "Virgin Islands (U.S.)",
    value: "VIR",
    id: 252,
  },
  {
    label: "Wallis and Futuna",
    value: "WLF",
    id: 253,
  },
  {
    label: "Western Sahara",
    value: "ESH",
    id: 254,
  },
  {
    label: "Yemen",
    value: "YEM",
    id: 255,
  },
  {
    label: "Zambia",
    value: "ZMB",
    id: 256,
  },
  {
    label: "Zimbabwe",
    value: "ZWE",
    id: 257,
  },
  {
    label: "Åland Islands",
    value: "ALA",
    id: 258,
  },
];
