export const AuthRoutes = {
  base: "authorization",
  SIGN_IN: "signin",
  SIGN_UP: "signup",
  REQUEST_PASSWORD: "request-password",
  RESET_PASSWORD: "reset-password",
  RESEND_CONFIRM: "resend-confirm",
  CONFIRM_EMAIL: "confirm-email",
  EMAIL_CONFIRMED: "email-confirmed",
  EMAIL_ALREADY_CONFIRMED: "email-already-confirmed",
};

export const routesList = {
  AUTH: AuthRoutes,
  SIGN_IN: "/",
  // SIGN_UP: "/sign-up",
  HOME: "/dashboard",
  PROFILE: "/profile",
  SETTINGS: "/settings",
  WALLETS: "/wallets",
};
