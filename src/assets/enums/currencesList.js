export const currencesData = [
  {
    label: "BTC",
    fullLabel: "Bitcoin",
    value: "tbtc",
  },
  {
    label: "ETH",
    fullLabel: "Ethereum",
    value: "teth",
  },
  {
    label: "BCH",
    fullLabel: "Bitcoin Cash",
    value: "tbch",
  },
/*
  {
    label: "LTC",
    fullLabel: "Litecoin",
    value: "tltc",
  },
  {
    label: "CRON",
    fullLabel: "CRON",
    value: "cron",
  },
  {
    label: "RUDT",
    fullLabel: "RUDT",
    value: "rudt",
  },
*/
];
