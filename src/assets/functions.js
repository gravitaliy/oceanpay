import moment from "moment";

export const renderSign = value => {
  if (value > 0)
    return {
      sign: "+",
      type: "Deposit",
      color: "text-green",
      img: require("./images/Positive.svg"),
    };
  if (value < 0)
    return {
      sign: "-",
      type: "Withdrawal",
      color: "text-red",
      img: require("./images/Negative.svg"),
    };
  return {
    sign: "",
    type: "",
    color: "",
    img: "",
  };
};

export const renderStatus = status => {
  switch (status) {
    case "AUTH":
      return {
        label: "Auth",
        color: "text-yellow",
      };
    case "SUCC":
      return {
        label: "Success",
        color: "text-green",
      };
    case "FAIL":
      return {
        label: "Fail",
        color: "text-red",
      };
    case "VOID":
      return {
        label: "Void",
        color: "text-red",
      };
    default:
      return {
        label: "",
        color: "",
      };
  }
};

export const renderCurrency = currency => {
  switch (currency) {
    case "USD":
      return "$";
    case "EUR":
      return "€";
    case "GBP":
      return "£";
    case "JPY":
      return "¥";
    case "CNY":
      return "¥";
    case "RUB":
      return "₽";
    default:
      return null;
  }
};

export const idPasswordConfirmation = "passwordConfirmation";

export const getDataWithoutPasswordConfirmation = data => {
  return Object.assign(
    ...Object.entries(data).map(item => {
      if (item[0] !== idPasswordConfirmation) return { [item[0]]: item[1] };
      return false;
    }),
  );
};

export const clearData = data => {
  return Object.assign(
    ...Object.entries(data).map(item => ({ [item[0]]: "" })),
  );
};

export const checkDate = text => {
  if (moment(text).isValid()) {
    return moment(text).format("DD.MM.YYYY");
  }
  return text;
};

export const initialValues = object => {
  return Object.assign(
    ...Object.entries(object).map(item => {
      if (item[1] !== "") return { [item[0]]: item[1] };
      return false;
    }),
  );
};

export const getNumberRu = (number, maximumFractionDigits = 6) => {
  return new Intl.NumberFormat("ru", {
    maximumFractionDigits,
  }).format(number);
};
