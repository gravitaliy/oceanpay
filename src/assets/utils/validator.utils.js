import { idPasswordConfirmation } from "../functions";

export const composeValidators = (...validators) => value =>
  validators.reduce((error, validator) => error || validator(value), undefined);

export const validator = (fn, message) => value =>
  fn(value) ? undefined : message;

export const validateEmail = email => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

export const validateConfirm = password => value => password === value;

export const required = value => value !== undefined;

/*
export const minLength = (length) => (value) =>
  value.length >= length;

export const maxLength = (length) => (value) =>
  value.length <= length;
*/

export const validateLatin = value => {
  const re = /^[A-Za-z\s]+$/;
  return re.test(String(value));
};

/*
export const validateNumeric = (value) => {
  const re = /^[0-9]+$/;
  return re.test(String(value));
};
*/

export const validateLatinNumeric = value => {
  const re = /^[A-Za-z0-9]+$/;
  return re.test(String(value));
};

export const validateDate = value => {
  const re = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
  return re.test(String(value));
};

export const validateCodePhone = value => {
  const re = /^\+?[0-9]{1,4}$/;
  return re.test(String(value).toLowerCase());
};

export const validatePhone = value => {
  const re = /^[0-9]{7,12}$/; // without code
  return re.test(String(value).toLowerCase());
};

export const onValidateRequired = (
  errors,
  values,
  value,
  text = "This field is required",
) => {
  errors[value] = validator(required, text)(values[value]);
};

export const onValidateLatin = (
  errors,
  values,
  value,
  isRequired,
  textLatin = "Enter only latin",
  textRequired = "This field is required",
) => {
  if (isRequired) {
    errors[value] = composeValidators(
      validator(required, textRequired),
      validator(validateLatin, textLatin),
    )(values[value]);
  } else {
    errors[value] = validator(validateLatin, textLatin)(values[value]);
  }
};

export const onValidateLatinNumeric = (
  errors,
  values,
  value,
  textRequired = "This field is required",
  textLatinNumeric = "Enter only numeric or latin",
) => {
  errors[value] = composeValidators(
    validator(required, textRequired),
    validator(validateLatinNumeric, textLatinNumeric),
  )(values[value]);
};

/*
export const onValidateNumeric = (errors, values, value, textRequired = "This field is required", textNumeric = "Enter only numeric") => {
  errors[value] = composeValidators(
    validator(required, textRequired),
    validator(validateNumeric, textNumeric),
  )(values[value]);
};
*/

export const onValidateDate = (
  errors,
  values,
  value,
  textRequired = "This field is required",
  textDate = "Enter correct date",
) => {
  errors[value] = composeValidators(
    validator(required, textRequired),
    validator(validateDate, textDate),
  )(values[value]);
};

export const onValidateCodePhone = (
  errors,
  values,
  value = "cell_number_country_code",
) => {
  errors[value] = composeValidators(
    validator(required, "Enter phone country code"),
    validator(validateCodePhone, "Enter correct phone country code"),
  )(values[value]);
};

export const onValidatePhone = (errors, values, value = "cell_number") => {
  errors[value] = composeValidators(
    validator(required, "Enter cell number"),
    validator(validatePhone, "Enter correct cell number"),
  )(values[value]);
};

export const onValidateEmail = (errors, values, value = "email") => {
  errors[value] = composeValidators(
    validator(required, "Enter email"),
    validator(validateEmail, "Enter correct email"),
  )(values[value]);
};

export const onValidatePassword = (errors, values, value = "password") => {
  errors[value] = validator(required, "Enter password")(values[value]);
};

export const onValidatePasswordConfirmation = (
  errors,
  values,
  oldPassword = "password",
) => {
  errors[idPasswordConfirmation] = composeValidators(
    validator(required, "Enter password confirmation"),
    validator(
      validateConfirm(values[oldPassword]),
      "Passwords are not identical",
    ),
  )(values[idPasswordConfirmation]);
};
