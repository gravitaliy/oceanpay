import format from "date-fns/format";

export const returnCardNumber = number =>
  returnCardNumberArray(number).join(" ");

export const returnCardNumberArray = number => number.match(/\d{1,4}/g);

export const returnTransactionDate = value =>
  format(new Date(value), "dd.MM.yyyy HH:mm");

export const returnLastFourNumbers = number => {
  const numberArray = returnCardNumberArray(number);
  return numberArray[numberArray.length - 1];
};
