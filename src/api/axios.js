import axios from "axios";
import { TOKEN } from "../assets/constans/auth.const";

const headers = config => ({
  ...config.headers,
  // "Grpc-Metadata-app-name": "test",
  // "Grpc-Metadata-auth-token": "2",
  "Grpc-Metadata-auth-token": localStorage.getItem(TOKEN),
  "Content-Type": "application/json",
  "x-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjA1MTgwOTgxfQ.LTp-lnxDKLrXY2vjyHiVpbgxdZCkv5IpYevfqg57MP4",
});

export const axiosInstance = axios.create({
  // baseURL: process.env.REACT_APP_API_URL || "https://api-cryptocard.cron.global",
});

axiosInstance.interceptors.request.use(config => {
  config.headers = headers(config);

  return config;
});

export const axiosInstanceAuth = axios.create({
  // baseURL: process.env.REACT_APP_AUTH_API_URL || "https://auth.cron.global",
});

axiosInstanceAuth.interceptors.request.use(config => {
  config.headers = headers(config);

  return config;
});
