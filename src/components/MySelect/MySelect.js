import React from "react";
import Select from "react-select";

export const MySelect = props => {
  const customStyles = {
    control: (styles, { isFocused }) => ({
      cursor: "pointer",
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-between",
      height: 50,
      backgroundColor: "#f4f5f8",
      borderWidth: 1,
      borderStyle: "solid",
      // eslint-disable-next-line no-nested-ternary
      borderColor: props.isError
        ? "#ff4891"
        : isFocused
        ? "#00c3ff"
        : "#c4c6d2",
      // border: "1px solid #c4c6d2",
      borderRadius: 25,
      boxShadow: "inset 0 0 250px rgba(255, 255, 255, 0.4)",
      transition: "border-color 0.2s ease-out",
      "&:hover": {
        borderColor: "#232c40",
      },
      // "&:focus": {
      //   outline: "none",
      //   borderColor: isFocused ? "#00c3ff",
      // },
    }),
    valueContainer: provided => ({
      ...provided,
      justifyContent: "center",
      fontSize: 20, // todo
    }),
    placeholder: provided => ({
      ...provided,
      color: props.isError && "#ff4891",
    }),
    menu: provided => ({
      ...provided,
      margin: 0,
      backgroundColor: "#f4f5f8",
      border: "1px solid #232c40",
      borderRadius: 25,
      boxShadow:
        "-14px 14px 60px rgba(139, 139, 139, 0.5), inset 0 0 250px rgba(255, 255, 255, 0.4)",
      overflow: "hidden",
    }),
    menuList: provided => ({
      ...provided,
      marginRight: 0,
      padding: 0,
      textAlign: "center",
      fontSize: 20, // todo
    }),
    option: (styles, { isDisabled, isFocused, isSelected }) => {
      return {
        ...styles,
        backgroundColor: null,
        // eslint-disable-next-line no-nested-ternary
        color: isDisabled
          ? "#ccc"
          : // eslint-disable-next-line no-nested-ternary
          isSelected
          ? "#232c40"
          : isFocused
          ? "#232c40"
          : "#a7a9b3",
        // eslint-disable-next-line no-nested-ternary
        cursor: isDisabled ? "not-allowed" : isSelected ? "default" : "pointer",
        ":active": null,
      };
    },
  };

  const IndicatorsContainer = () => {
    return (
      <div
        style={{
          display: "flex",
          flexShrink: 0,
          alignSelf: "stretch",
          alignItems: "center",
          marginRight: 5,
        }}
      >
        <div
          style={{
            display: "flex",
            padding: 8,
          }}
        >
          <img
            style={{
              width: 25,
              transform: "rotate(180deg)",
            }}
            src={require("./assets/arrow.svg")}
            alt=""
          />
        </div>
      </div>
    );
  };

  return (
    <Select
      name={props.id}
      styles={customStyles}
      value={props.data.filter(option => option.value === props.value)}
      onChange={props.onChange}
      options={props.data}
      isSearchable={props.isSearchable || false}
      components={{
        IndicatorSeparator: () => null,
        IndicatorsContainer,
      }}
    />
  );
};
