import React, { memo, useState } from "react";
import clsx from "clsx";

// local files
import { MySelect } from "../MySelect/MySelect";
import { MyFileUpload } from "../MyFileUpload/MyFileUpload";
import "./MyInput.scss";

export const MyInput = memo(
  ({
    label,
    required,
    id,
    idUrl,
    type,
    data,
    isSearchable,
    value,
    valueUrl,
    onChange,
    onChangeSelect,
    onChangeFile,
    name,
    className,
    meta,
    ...props
  }) => {
    const [controlType, setControlType] = useState(type);

    const handleShowPassword = () => {
      setControlType(controlType === "password" ? "text" : "password");
    };

    const isError = meta?.error && meta?.touched;

    const renderInput = () => {
      switch (type) {
        case "select":
          return (
            <MySelect
              id={id}
              data={data}
              isSearchable={isSearchable}
              onChange={onChangeSelect}
              value={value}
              isError={isError}
            />
          );
        case "file":
          return (
            <MyFileUpload
              id={id}
              idUrl={idUrl}
              valueUrl={valueUrl}
              onChangeFile={onChangeFile}
              isError={isError}
            />
          );
        default:
          return (
            <input
              name={id}
              className={clsx(
                "h1",
                "my-input__input",
                type === "password" && "password",
                isError && "error",
              )}
              id={id}
              type={controlType || "text"}
              required={required || false}
              value={value}
              onChange={onChange}
              /* eslint-disable-next-line react/jsx-props-no-spreading */
              {...props}
            />
          );
      }
    };

    return (
      <div className="my-input">
        {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
        <label className="h1 text-grey my-input__label" htmlFor={id}>
          {label}
          {required && " *"}
        </label>
        <div className="my-input__input-wrap">
          {renderInput()}
          {type === "password" && (
            <button
              className="default my-input__show-password"
              type="button"
              onClick={handleShowPassword}
            >
              {controlType === "password" ? (
                <img src={require("./assets/eye-open.svg")} alt="" />
              ) : (
                <img src={require("./assets/eye-close.svg")} alt="" />
              )}
            </button>
          )}
        </div>
        {isError && <div className="my-input__error-text">{meta.error}</div>}
      </div>
    );
  },
);
