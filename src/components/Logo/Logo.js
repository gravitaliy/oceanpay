import React from "react";
import { Link } from "react-router-dom";

// local files
import { routesList } from "../../assets/enums";
import "./Logo.scss";

export const Logo = () => {
  return (
    <Link className="c-logo" to={routesList.SIGN_IN}>
      <img src={require("./assets/logo.svg")} alt="logo" />
    </Link>
  );
};
