import React, { useEffect } from "react";
import { inject, observer } from "mobx-react";
import { MyInput } from "../../MyInput/MyInput";
import { MyButton } from "../../MyButton/MyButton";
import { CardItem } from "../../CardItem/CardItem";
import { MyLoader } from "../../MyLoader/MyLoader";
import "./ModalContent.scss";

export const ModalContentTransfer = inject("store")(
  observer(({ activeCard, isKycApproved, store: { transfer } }) => {
    const handleInputChange = e => {
      if (!Number.isNaN(Number(e.target.value))) {
        transfer.setValue(e.target.id, e.target.value);
      }
    };

    const handleSubmit = e => {
      e.preventDefault();
      transfer.sendTransfer();
    };

    useEffect(() => {
      transfer.setValue("from_card_number", activeCard.number);
    }, [activeCard.number]);

    return (
      <form onSubmit={handleSubmit}>
        <div className="modal-inner">
          {isKycApproved ? (
            <>
              {transfer.loading && <MyLoader transparent />}
              <div className="h1">Card to Card Transfer</div>
              <div className="modal-card-wrap">
                <CardItem size="xs" activeCard={activeCard} />
                <div className="modal-card-text">
                  <div>Card Balance</div>
                  <div className="h1 text-grey">{`$ ${activeCard.balance}`}</div>
                </div>
              </div>
              <div className="modal__inputs-wrap">
                <MyInput
                  label="Card Number"
                  id="to_card_number"
                  value={transfer.inputs.to_card_number}
                  onChange={handleInputChange}
                  pattern="[0-9]*"
                  title="Write only numbers"
                  maxLength="16"
                  minLength="16"
                  placeholder="Enter card number"
                  required
                />
                <MyInput
                  label="Amount"
                  id="amount"
                  type="number"
                  value={transfer.inputs.amount}
                  onChange={handleInputChange}
                  pattern="[0-9]*"
                  min="0.1"
                  step="0.1"
                  max={activeCard.balance}
                  title="Write only numbers"
                  placeholder="Enter amount"
                  required
                />
              </div>
              <MyButton
                className="modal__submit-btn"
                type="submit"
                value="Submit transfer"
              />
            </>
          ) : (
            <div className="h1 text-red">
              You can&apos;t make transfers until you have passed KYC
            </div>
          )}
        </div>
      </form>
    );
  }),
);
