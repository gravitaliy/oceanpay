import React, { memo } from "react";
import "./ModalContent.scss";

export const ModalContentPin = memo(({ pin }) => {
  return (
    <div className="modal-inner">
      <div className="h1 modal-title">Your PIN CODE</div>
      <div className="modal-pin text-red">{pin}</div>
    </div>
  );
});
