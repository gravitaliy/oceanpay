import React, { memo, useState } from "react";
import { MyInput } from "../../MyInput/MyInput";
import { MyButton } from "../../MyButton/MyButton";
import "./ModalContent.scss";

export const ModalContentAdd = memo(({ onSubmit }) => {
  const [inputValue, setInputValue] = useState("");

  const onInputChange = e => {
    if (!Number.isNaN(Number(e.target.value))) {
      setInputValue(e.target.value);
    }
  };

  return (
    <form onSubmit={onSubmit(inputValue)}>
      <div className="modal-inner">
        <div className="modal__inputs-wrap">
          <MyInput
            label="Card Number"
            value={inputValue}
            onChange={onInputChange}
            pattern="[0-9]*"
            title="Write only numbers"
            maxLength="16"
            minLength="16"
            placeholder="Enter card number"
            required
          />
        </div>
        <MyButton className="modal__submit-btn" type="submit" value="Enroll" />
      </div>
    </form>
  );
});
