import React from "react";
import { inject, observer } from "mobx-react";
import Modal from "react-modal";

// local
import "./Modals.scss";

export const Modals = inject("store")(
  observer(({ onCloseClick, isOpen, innerContent }) => {
    return (
      <Modal
        isOpen={!!isOpen}
        onRequestClose={onCloseClick}
        className="modal"
        ariaHideApp={false}
        style={{
          overlay: {
            zIndex: 3,
            backgroundColor: "rgba(0, 0, 0, 0.75)",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          },
        }}
      >
        <button
          type="button"
          className="default modal__close-btn"
          onClick={onCloseClick}
        >
          <img src={require("./assets/close.svg")} alt="" />
        </button>
        {innerContent}
      </Modal>
    );
  }),
);
