import React, { memo, useEffect, useState } from "react";
import Files from "react-butterfiles";
import clsx from "clsx";

// local files
import { MyButton } from "../MyButton/MyButton";
import "./MyFileUpload.scss";

export const MyFileUpload = memo(
  ({ id, idUrl, valueUrl, onChangeFile, isError }) => {
    const [files, setFiles] = useState([]);
    const [url, setUrl] = useState("");
    const [base64, setBase64] = useState("");
    const [errors, setErrors] = useState([]);

    const handleSuccess = files => {
      setFiles(files);
      setBase64(files[0].src.base64);
      onChangeFile(files[0].src.base64, id, idUrl);
    };

    const handleError = errors => {
      setErrors(errors);
    };

    const handleClearFiles = () => {
      setFiles([]);
      setUrl("");
      setBase64("");
      onChangeFile("", id, idUrl);
    };

    useEffect(() => {
      setUrl(valueUrl);
    }, [valueUrl]);

    return (
      <>
        <Files
          convertToBase64
          multiple={false}
          maxSize="2mb"
          accept={["image/jpg", "image/jpeg", "image/png", "image/gif"]}
          onSuccess={handleSuccess}
          onError={handleError}
        >
          {({ browseFiles }) => (
            <div className="file">
              <div className="file__btn-wrap">
                <MyButton
                  value="Select File"
                  onClick={browseFiles}
                  variant="button-file"
                  className={clsx(isError && "button-error")}
                />
              </div>
              {files.map(file => (
                <div className="file__text" key={file.name}>
                  {file.name}
                </div>
              ))}
              {errors.map(error => (
                <div className="file__text" key={error.file.name}>
                  {error.file.name} - {error.type}
                </div>
              ))}
            </div>
          )}
        </Files>
        {(!!base64 || !!url) && (
          <div className="file__preview-wrap">
            <button
              type="button"
              className="default file__preview-wrap__close"
              onClick={handleClearFiles}
            >
              <img src={require("./assets/close.svg")} alt="" />
            </button>
            <div className="file__preview-img-wrap">
              <img style={{ width: "100%" }} src={base64 || url} alt="" />
            </div>
          </div>
        )}
      </>
    );
  },
);
