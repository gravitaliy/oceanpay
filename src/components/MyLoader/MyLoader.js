import React from "react";
import Loader from "react-loader-spinner";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import clsx from "clsx";

// local files
import "./MyLoader.scss";

export const MyLoader = ({ transparent, style }) => {
  return (
    <div
      className={clsx(
        "loader",
        transparent ? "loader-transparent" : "loader-general",
      )}
      style={style}
    >
      <Loader type="Oval" color="#00BFFF" height={100} width={100} />
    </div>
  );
};
