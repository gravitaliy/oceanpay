import React from "react";
import "./AddButton.scss";

export const AddButton = ({ onClick }) => {
  return (
    <button onClick={onClick} type="button" className="add-button">
      <span className="add-button__plus">+</span>
      <span className="add-button__text">Add card</span>
    </button>
  );
};
