import React from "react";
import SortableTable from "react-sortable-table";
import clsx from "clsx";

// local files
import {
  renderCurrency,
  renderSign,
  renderStatus,
} from "../../assets/functions";
import { returnTransactionDate } from "../../assets/utils/dashboard.utils";
import "./MyTable.scss";

window.React = require("react");

export const MyTable = ({ data }) => {
  const columns = [
    {
      header: "Operation type",
      key: "amount",
      render: value => (
        <div className={clsx(renderSign(value).color)}>
          {renderSign(value).type}
        </div>
      ),
    },
    {
      header: "Merchant",
      key: "merchant",
    },
    {
      header: "Date",
      key: "executed_at",
      defaultSorting: "DESC",
      render: value => returnTransactionDate(value),
    },
    {
      header: "Amount",
      key: "reference",
      render: value => {
        const { amount, currency } = data.find(item => value === item.reference);
        return (
          <div className={clsx(renderSign(amount).color)}>
            {renderSign(amount).sign +
              renderCurrency(currency) +
              Math.abs(amount)}
          </div>
        );
      },
    },
    {
      header: "Status",
      key: "transaction_status",
      render: value => (
        <div className={clsx(renderStatus(value).color)}>
          {renderStatus(value).label}
        </div>
      ),
    },
    /*
    {
      header: "Currency 2",
      key: "amount",
      render: value => (
        <div className={clsx(renderSign(value).color)}>
          {`${renderSign(value).sign}$${Math.abs(value)}`}
        </div>
      ),
    },
    */
  ].map(item => ({
    ...item,
    headerProps: { className: clsx("table-head", `table-head--${item.key}`) },
    dataProps: { className: "table-data" },
  }));

  return <SortableTable data={data} columns={columns} />;
};
