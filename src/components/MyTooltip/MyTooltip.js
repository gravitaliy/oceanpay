import React, { memo } from "react";
import ReactTooltip from "react-tooltip";

export const MyTooltip = memo(({ value, id, children, event }) => {
  return (
    <>
      <div data-tip={value} data-for={id}>
        {children}
      </div>
      <ReactTooltip
        id={id}
        place="bottom"
        effect="solid"
        event={event || ""}
        globalEventOff="click"
      />
    </>
  );
});
