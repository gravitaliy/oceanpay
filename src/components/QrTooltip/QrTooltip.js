import React, { memo } from "react";
import ReactTooltip from "react-tooltip";
import QRCode from "qrcode.react";

export const QrTooltip = memo(({ qrValue, id, children }) => {
  return (
    <>
      <div data-tip="" data-event="click" data-for={id}>
        {children}
      </div>
      <ReactTooltip
        id={id}
        place="top"
        type="light"
        effect="solid"
        globalEventOff="click"
      >
        <QRCode value={qrValue} size={200} />
      </ReactTooltip>
    </>
  );
});
