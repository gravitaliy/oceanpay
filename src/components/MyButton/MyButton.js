import React, { memo } from "react";
import clsx from "clsx";

// local files
import "./MyButton.scss";

export const MyButton = memo((props) => {
  return (
    // eslint-disable-next-line react/button-has-type
    <button
      className={clsx("button", props.className, props.variant)}
      type={props.type || "button"}
      onClick={props.onClick}
    >
      {props.icon && <img className="button-icon" src={props.icon} alt="" />}
      {props.iconRender && (
        <div className="button-icon">{props.iconRender}</div>
      )}
      <div>{props.value}</div>
    </button>
  );
});
