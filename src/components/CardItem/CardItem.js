import React from "react";
import clsx from "clsx";

// local files
import { returnLastFourNumbers } from "../../assets/utils/dashboard.utils";
import "./CardItem.scss";

export const CardItem = ({ activeCard, size }) => {
  return (
    <div className={clsx("my-card", size)}>
      {activeCard.number && (
        <div className="h1 my-card__number">
          ... {returnLastFourNumbers(activeCard.number)}
        </div>
      )}
      <div className="my-card__name">{activeCard.name}</div>
      <div className="my-card__type">
        <img src={require("./assets/master.png")} alt="" />
      </div>
    </div>
  );
};
