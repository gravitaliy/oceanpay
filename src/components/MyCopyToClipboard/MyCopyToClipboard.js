import React, { memo, useState } from "react";
import { CopyToClipboard } from "react-copy-to-clipboard";
import clsx from "clsx";

// local files
import "./MyCopyToClipboard.scss";

export const MyCopyToClipboard = memo(({ value, copyValue, className }) => {
  const [copied, setCopied] = useState(false);

  const onCopy = () => {
    setCopied(true);
    setTimeout(() => {
      setCopied(false);
    }, 2000);
  };

  return (
    <span className={clsx("copy-wrap", className)}>
      <span className="copy-value">{value}</span>
      <CopyToClipboard onCopy={onCopy} text={copyValue || value}>
        <div className="copy-btn-wrap">
          <button className="default copy-btn" type="button">
            <img src={require("./assets/copy.svg")} alt="" />
          </button>
          <span className={clsx("copy-done-text", copied && "active")}>
            Copied!
          </span>
        </div>
      </CopyToClipboard>
    </span>
  );
});
