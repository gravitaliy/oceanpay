import React from "react";
import "./DropdownWrap.scss";

export const DropdownWrap = data => {
  return (
    <div className="dropdown-content">
      <button type="button" className="default dropdown-content__close-btn">
        <img src={require("./assets/close.svg")} alt="" />
      </button>
      {data.map(item =>
        item.onClick ? (
          <button
            className="h1 default dropdown-content-item-btn"
            type="button"
            onClick={item.onClick}
            key={item.id}
          >
            {item.label}
          </button>
        ) : (
          <div className="h1 dropdown-content-item-text" key={item.id}>
            {item.label}
          </div>
        ),
      )}
    </div>
  );
};
