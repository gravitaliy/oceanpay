const { createProxyMiddleware } = require("http-proxy-middleware");

module.exports = app => {
  app.use(
    "/Cryptocard",
    createProxyMiddleware({
      target:
        process.env.REACT_APP_API_URL || "https://api-cryptocard.cron.global",
      changeOrigin: true,
    }),
  );
  app.use(
    "/Auth",
    createProxyMiddleware({
      target: process.env.REACT_APP_AUTH_API_URL || "https://auth.cron.global",
      changeOrigin: true,
    }),
  );
  app.use(
    "/v1",
    createProxyMiddleware({
      target: "https://dev-api-bitcrypto.bitchange.online",
      changeOrigin: true,
    })
  );
};
