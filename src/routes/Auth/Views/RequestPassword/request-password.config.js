import { routesList } from "../../../../assets/enums";
import { onValidateEmail } from "../../../../assets/utils/validator.utils";

export const validate = values => {
  const errors = {};
  onValidateEmail(errors, values);
  return errors;
};

export const formData = [
  {
    label: "E-mail",
    type: "email",
    id: "email",
  },
];

export const linksData = [
  {
    link: routesList.AUTH.SIGN_UP,
    label: "Sign up",
  },
  {
    link: routesList.SIGN_IN,
    label: "Log in",
  },
].map((item, i) => ({ ...item, id: i }));
