import React from "react";
import { inject, observer } from "mobx-react";

// local files
import { AuthLayout } from "../../components";
import { formData, linksData, validate } from "./request-password.config";

export const RequestPassword = inject("store")(
  observer(({ store: { auth } }) => {
    const onSubmit = () => {
      auth.sendRequestPassword();
    };

    return (
      <AuthLayout
        title="Forgot your password?"
        titleBtn="Request password"
        formData={formData}
        linksData={linksData}
        values="requestPassword"
        onSubmit={onSubmit}
        validate={validate}
      />
    );
  }),
);
