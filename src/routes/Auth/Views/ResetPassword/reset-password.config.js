import { onValidatePassword } from "../../../../assets/utils/validator.utils";

export const validate = values => {
  const errors = {};
  onValidatePassword(errors, values, "new_password");
  return errors;
};

export const formData = [
  {
    label: "New password",
    type: "password",
    id: "new_password",
  },
];
