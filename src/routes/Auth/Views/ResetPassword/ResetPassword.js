import React, { useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";
import qs from "qs";
import { inject, observer } from "mobx-react";

// local files
import { AuthLayout } from "../../components";
import { formData, validate } from "./reset-password.config";
import { routesList } from "../../../../assets/enums";

export const ResetPassword = inject("store")(
  observer(({ store: { auth } }) => {
    const history = useHistory();
    const location = useLocation();

    const getQuery = qs.parse(location.search, {
      ignoreQueryPrefix: true,
    });

    useEffect(() => {
      auth.setRestorePassword("email", getQuery.email);
      auth.setRestorePassword("restore_code", getQuery.restore_code);
    }, []);

    const onSubmit = () => {
      const redirect = () => history.push(routesList.SIGN_IN);
      auth.sendRestorePassword(redirect);
    };

    return (
      <AuthLayout
        title="Enter a new password"
        titleBtn="Reset password"
        formData={formData}
        values="resetPassword"
        onSubmit={onSubmit}
        validate={validate}
      />
    );
  }),
);
