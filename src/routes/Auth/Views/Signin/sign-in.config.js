import { routesList } from "../../../../assets/enums";
import {
  onValidateEmail,
  onValidatePassword,
} from "../../../../assets/utils/validator.utils";

export const validate = values => {
  const errors = {};
  onValidateEmail(errors, values, "email");
  onValidatePassword(errors, values, "password");
  return errors;
};

export const formData = [
  {
    label: "E-mail",
    type: "email",
    id: "email",
  },
  {
    label: "Password",
    type: "password",
    id: "password",
  },
];

export const linksData = [
  {
    link: routesList.AUTH.SIGN_UP,
    label: "Sign up",
  },
  {
    link: routesList.AUTH.REQUEST_PASSWORD,
    label: "Forgot your password?",
  },
  /*
  {
    link: "/",
    label: "Didn't receive confirmation instructions?",
  },
  */
].map((item, i) => ({ ...item, id: i }));
