import React from "react";
import { inject, observer } from "mobx-react";
import { useHistory } from "react-router-dom";

// local files
import { formData, linksData, validate } from "./sign-in.config";
import { routesList } from "../../../../assets/enums";
import { AuthLayout } from "../../components";

export const Signin = inject("store")(
  observer(({ store: { auth } }) => {
    const history = useHistory();

    const onSubmit = () => {
      const redirect = () => history.push(routesList.HOME);
      auth.sendSignIn(redirect);
    };

    return (
      <AuthLayout
        title="Log in"
        formData={formData}
        linksData={linksData}
        values="signIn"
        onSubmit={onSubmit}
        validate={validate}
      />
    );
  }),
);
