import React from "react";
import { useHistory } from "react-router-dom";
import { inject, observer } from "mobx-react";

// local files
import { AuthLayout } from "../../components";
import { formData, linksData, validate } from "./sign-up.config";
import { routesList } from "../../../../assets/enums";

export const Signup = inject("store")(
  observer(({ store: { auth } }) => {
    const history = useHistory();

    const onSubmit = () => {
      const redirect = () => history.push(routesList.SIGN_IN);
      auth.sendSignUp(redirect);
    };

    return (
      <AuthLayout
        title="Sign up"
        formData={formData}
        linksData={linksData}
        values="signUp"
        onSubmit={onSubmit}
        validate={validate}
      />
    );
  }),
);
