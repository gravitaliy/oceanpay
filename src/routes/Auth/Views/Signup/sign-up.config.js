import { routesList } from "../../../../assets/enums";
import { idPasswordConfirmation } from "../../../../assets/functions";
import {
  onValidateEmail,
  onValidatePassword,
  onValidatePasswordConfirmation,
  onValidateRequired,
} from "../../../../assets/utils/validator.utils";

export const validate = values => {
  const errors = {};
  onValidateRequired(errors, values, "login", "Enter login");
  onValidateEmail(errors, values);
  onValidatePassword(errors, values);
  onValidatePasswordConfirmation(errors, values);
  return errors;
};

export const formData = [
  {
    label: "Login",
    id: "login",
  },
  {
    label: "E-mail",
    type: "email",
    id: "email",
  },
  {
    label: "Password",
    type: "password",
    id: "password",
  },
  {
    label: "Password confirmation",
    type: "password",
    id: idPasswordConfirmation,
  },
];

export const linksData = [
  {
    link: routesList.SIGN_IN,
    label: "Log in",
  },
  {
    link: routesList.AUTH.RESEND_CONFIRM,
    label: "Resend confirmation email",
  },
  /*
  {
    link: "/",
    label: "Didn't receive confirmation instructions?",
  },
  */
].map((item, i) => ({ ...item, id: i }));
