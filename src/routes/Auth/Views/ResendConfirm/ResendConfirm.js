import React from "react";
import { inject, observer } from "mobx-react";

// local files
import { AuthLayout } from "../../components";
import { formData, linksData, validate } from "./resend-confirm.config";

export const ResendConfirm = inject("store")(
  observer(({ store: { auth } }) => {
    const onSubmit = () => {
      auth.sendResendVerificationMail();
    };

    return (
      <AuthLayout
        title="Resend confirmation email"
        titleBtn="Resend"
        formData={formData}
        linksData={linksData}
        values="requestPassword"
        onSubmit={onSubmit}
        validate={validate}
      />
    );
  }),
);
