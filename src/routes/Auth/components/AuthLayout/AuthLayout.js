import React from "react";
import { Link } from "react-router-dom";
import { inject, observer } from "mobx-react";
import { Field, Form } from "react-final-form";

// local files
import { MyInput, MyButton, MyLoader } from "../../../../components";
import { initialValues } from "../../../../assets/functions";
import "./AuthLayout.scss";

export const AuthLayout = inject("store")(
  observer(
    ({
      title,
      titleBtn,
      formData,
      linksData = [],
      onSubmit,
      values,
      validate,
      store: { auth },
    }) => {
      const { loading } = auth;

      const changeHandler = input => e => {
        input.onChange(e);
        auth.setValue(values, {
          ...auth[values],
          [e.target.id]: e.target.value,
        });
      };

      return (
        <main className="main auth-main">
          <div className="container auth-container">
            <div className="auth-inner">
              <div className="auth-card">
                <div className="title">{title}</div>
                <div className="auth-form-wrap">
                  <Form
                    onSubmit={onSubmit}
                    validate={validate}
                    initialValues={initialValues(auth[values])}
                    render={({ handleSubmit }) => (
                      <form onSubmit={handleSubmit} noValidate>
                        {formData.map(item => (
                          <Field name={item.id} key={item.id}>
                            {({ input, meta }) => (
                              <MyInput
                                /* eslint-disable-next-line react/jsx-props-no-spreading */
                                {...input}
                                label={item.label}
                                id={item.id}
                                type={item.type}
                                value={auth[values][item.id]}
                                onChange={changeHandler(input)}
                                required
                                meta={meta}
                              />
                            )}
                          </Field>
                        ))}
                        <div className="auth-submit-wrap">
                          <MyButton
                            value={titleBtn || title}
                            type="submit"
                            className="auth-submit-btn"
                          />
                        </div>
                      </form>
                    )}
                  />
                  <div className="auth-links">
                    {linksData.map(item => (
                      <Link
                        to={item.link}
                        className="h1 text-grey auth-links__item"
                        key={item.id}
                      >
                        {item.label}
                      </Link>
                    ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
          {loading && <MyLoader transparent />}
        </main>
      );
    },
  ),
);
