import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

// local files
import { routesList } from "../../assets/enums";
import { Signin, Signup, RequestPassword, ResetPassword, ResendConfirm } from "./Views/Views";

const { AUTH: { base, SIGN_IN, SIGN_UP, REQUEST_PASSWORD, RESET_PASSWORD, RESEND_CONFIRM } } = routesList;
export const Auth = () => {
  return (
    <Switch>
      <Route path={`/${base}`} exact>
        <Redirect
          from={`/${base}`}
          to={`/${base}/${SIGN_IN}`}
        />
      </Route>
      {/* ------------------------ Страница Входа  ------------------------ */}
      <Route path={`/${base}/${SIGN_IN}`}>
        <Signin />
      </Route>
      {/* ------------------------ Страница Регистрации  ------------------------ */}
      <Route path={`/${base}/${SIGN_UP}`}>
        <Signup />
      </Route>
      {/* ------------------------ Страница Забыли пароль  ------------------------ */}
      <Route
        path={`/${base}/${REQUEST_PASSWORD}`}
      >
        <RequestPassword />
      </Route>
      {/* ------------------------ Страница Восстановления пароля  ------------------------ */}
      <Route
        path={`/${base}/${RESET_PASSWORD}`}
      >
        <ResetPassword />
      </Route>
      {/* ------------------------ Страница Resend confirmation email  ------------------------ */}
      <Route
        path={`/${base}/${RESEND_CONFIRM}`}
      >
        <ResendConfirm />
      </Route>
    </Switch>
  );
};
