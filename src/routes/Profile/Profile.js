import React, { useEffect } from "react";
import { inject, observer } from "mobx-react";
import { Field, Form } from "react-final-form";

// local files
import { MyInput, MyButton, MyLoader } from "../../components";
import { formData, validate } from "./profile.config";
import { initialValues } from "../../assets/functions";
import "./Profile.scss";

export const Profile = inject("store")(
  observer(({ store: { profile, dashboard, auth } }) => {
    const { cardsList } = dashboard;
    const { token } = auth;

    useEffect(() => {
      dashboard.getCardsListInfo();
    }, [token]);

    useEffect(() => {
      if (cardsList?.length) {
        profile.setCardNumber(cardsList[0]?.card_info?.number);
        profile.getUserProfileInfo();
      }
    }, [cardsList, token]);

    const handleChangeInput = input => e => {
      input.onChange(e);
      profile.setValue(e.target.id, e.target.value);
    };

    const handleChangeSelect = input => (value, item) => {
      input.onChange(value.value);
      profile.setValue(item.name, value.value);
    };

    const handleChangeFile = input => (base64, target, targetUrl) => {
      input.onChange(base64);
      profile.setValue(target, base64);
      profile.setValue(targetUrl, "");
    };

    const handleSendProfile = () => {
      profile.sendProfile();
    };

    return (
      <main className="main">
        <div className="container">
          <div className="title">Profile</div>
          <div className="h1 text-red c-profile-subtitle">
            All applications are processed in work time from 8:00 to 17:00 UTC+8
          </div>
          <Form
            onSubmit={handleSendProfile}
            validate={validate}
            initialValues={initialValues(profile.inputs.user_info_for_update)}
            render={({ handleSubmit }) => (
              <form onSubmit={handleSubmit} noValidate>
                {formData.map(item => (
                  <div className="form-item-wrap" key={item.id}>
                    <div className="h1 form-item-title">{item.title}</div>
                    {item.inputs.map((row, i) => (
                      <div className="form-row" key={i}>
                        {row.map(myInput => {
                          if (
                            profile.inputs.user_info_for_update.doc_pid_type !==
                              "ID_CARD" &&
                            myInput.id === "doc_pidback_image_base64"
                          ) {
                            return null;
                          }
                          return (
                            <div
                              className={`form-col--${row.length}`}
                              key={myInput.id}
                            >
                              <Field name={myInput.id}>
                                {({ input, meta }) => (
                                  <MyInput
                                    /* eslint-disable-next-line react/jsx-props-no-spreading */
                                    {...input}
                                    id={myInput.id}
                                    idUrl={myInput.idUrl}
                                    label={myInput.label}
                                    required={myInput.required}
                                    data={myInput.data}
                                    type={myInput.type}
                                    isSearchable={myInput.isSearchable}
                                    value={
                                      profile.inputs.user_info_for_update[
                                        myInput.id
                                      ]
                                    }
                                    valueUrl={
                                      profile.inputs.user_info_for_update[
                                        myInput.idUrl
                                      ]
                                    }
                                    onChange={handleChangeInput(input)}
                                    onChangeSelect={handleChangeSelect(input)}
                                    onChangeFile={handleChangeFile(input)}
                                    meta={meta}
                                  />
                                )}
                              </Field>
                            </div>
                          );
                        })}
                      </div>
                    ))}
                  </div>
                ))}
                <div className="form-submit-wrap">
                  <div className="form-submit-text">
                    Fields marked with asterisk (*) are mandatory
                  </div>
                  <MyButton
                    className="form-submit-btn"
                    value="Submit to Check"
                    type="submit"
                  />
                </div>
              </form>
            )}
          />
        </div>
        {profile.loading && <MyLoader transparent />}
      </main>
    );
  }),
);
