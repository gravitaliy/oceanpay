import {
  genderList,
  occupationList,
  countryList,
  docTypeList,
} from "../../assets/enums";
import {
  onValidateCodePhone,
  onValidateEmail,
  onValidatePhone,
  onValidateLatin,
  onValidateRequired,
  onValidateDate,
  onValidateLatinNumeric,
} from "../../assets/utils/validator.utils";

export const validate = values => {
  const errors = {};
  onValidateEmail(errors, values);
  onValidatePhone(errors, values);
  onValidateCodePhone(errors, values);
  onValidateLatin(errors, values, "firstname", true);
  onValidateLatin(errors, values, "lastname", true);
  onValidateLatin(errors, values, "middle_name", false);
  onValidateRequired(errors, values, "gender");
  onValidateRequired(errors, values, "nationality");
  onValidateRequired(errors, values, "occupation");
  onValidateDate(errors, values, "date_of_birth");
  onValidateRequired(errors, values, "address");
  onValidateRequired(errors, values, "city");
  onValidateLatinNumeric(errors, values, "postal_code");
  onValidateRequired(errors, values, "country");
  onValidateRequired(errors, values, "doc_pid_type");
  onValidateLatinNumeric(errors, values, "doc_pid_number");
  onValidateDate(errors, values, "doc_pid_issue_at");
  onValidateDate(errors, values, "doc_pid_expired_at");

  if (values.doc_pid_image_url) {
    onValidateRequired(errors, values, "doc_pid_image_url");
  } else {
    onValidateRequired(errors, values, "doc_pid_image_base64");
  }

  if (values.doc_pid_type === "ID_CARD") {
    if (values.doc_pidback_image_url) {
      onValidateRequired(errors, values, "doc_pidback_image_url");
    } else {
      onValidateRequired(errors, values, "doc_pidback_image_base64");
    }
  }

  if (values.doc_verification_image_url) {
    onValidateRequired(errors, values, "doc_verification_image_url");
  } else {
    onValidateRequired(errors, values, "doc_verification_image_base64");
  }

  if (values.doc_sign_image_url) {
    onValidateRequired(errors, values, "doc_sign_image_url");
  } else {
    onValidateRequired(errors, values, "doc_sign_image_base64");
  }

  return errors;
};

export const formData = [
  {
    title: "Contact Information",
    inputs: [
      [
        {
          label: "E-mail",
          id: "email",
          required: true,
        },
      ],
      [
        {
          label: "Cell number country code",
          type: "tel",
          id: "cell_number_country_code",
          required: true,
        },
        {
          label: "Cell number",
          type: "tel",
          id: "cell_number",
          required: true,
        },
      ],
    ],
  },
  {
    title: "Personal Information",
    inputs: [
      [
        {
          label: "First Name (English)",
          id: "firstname",
          required: true,
        },
        {
          label: "Last Name (English)",
          id: "lastname",
          required: true,
        },
      ],
      [
        {
          label: "Middle Name (English)",
          id: "middle_name",
        },
        {
          label: "Date of Birth",
          id: "date_of_birth",
          required: true,
        },
      ],
      [
        {
          label: "First Name (native language)",
          id: "firstname_local",
        },
        {
          label: "Last Name (native language)",
          id: "lastname_local",
        },
      ],
      [
        {
          label: "Gender",
          id: "gender",
          required: true,
          type: "select",
          data: genderList,
        },
        {
          label: "Nationality",
          id: "nationality",
          type: "select",
          data: countryList,
          required: true,
          isSearchable: true,
        },
        {
          label: "Occupation",
          id: "occupation",
          required: true,
          type: "select",
          data: occupationList,
        },
      ],
    ],
  },
  {
    title: "Address Information",
    inputs: [
      [
        {
          label: "Address",
          id: "address",
          required: true,
        },
        {
          label: "City",
          id: "city",
          required: true,
        },
      ],
      [
        {
          label: "Postal Code",
          id: "postal_code",
          required: true,
        },
        {
          label: "Country",
          id: "country",
          required: true,
          type: "select",
          data: countryList,
          isSearchable: true,
        },
      ],
    ],
  },
  {
    title: "ID Document / Passport",
    inputs: [
      [
        {
          label: "Document Type",
          id: "doc_pid_type",
          type: "select",
          data: docTypeList,
          required: true,
        },
        {
          label: "Document Number",
          id: "doc_pid_number",
          required: true,
        },
      ],
      [
        {
          label: "Issue at",
          id: "doc_pid_issue_at",
          required: true,
        },
        {
          label: "Expired at",
          id: "doc_pid_expired_at",
          required: true,
        },
      ],
      [
        {
          label: "Image Front Side",
          id: "doc_pid_image_base64",
          idUrl: "doc_pid_image_url",
          required: true,
          type: "file",
        },
        {
          label: "Image Back Side",
          id: "doc_pidback_image_base64",
          idUrl: "doc_pidback_image_url",
          required: true,
          type: "file",
        },
      ],
      [
        {
          label: "Selfie Photo",
          id: "doc_verification_image_base64",
          idUrl: "doc_verification_image_url",
          required: true,
          type: "file",
        },
        {
          label: "Sign",
          id: "doc_sign_image_base64",
          idUrl: "doc_sign_image_url",
          required: true,
          type: "file",
        },
      ],
    ],
  },
].map((item, i) => ({ ...item, id: i }));

/*
export const formDataDocs = [
  /!*
  [
    {
      title: "ID Document / Passport",
      inputs: [
        {
          label: "Document Type",
          id: "IdDocumentType",
          required: true,
        },
        {
          label: "Document Number",
          id: "IdDocumentNumber",
          required: true,
        },
        {
          label: "Image File",
          id: "IdImageFile",
          required: true,
        },
      ]
    },
    {
      title: "ID Document Back Side",
      inputs: [
        {
          label: "Document Type",
          id: "backDocumentType",
        },
        {
          label: "Document Number",
          id: "backDocumentNumber",
        },
        {
          label: "Image File",
          id: "backImageFile",
        },
      ]
    }
  ],
  *!/
  [
    {
      title: "Selfie Photo",
      inputs: [
        // {
        //   label: "doc_verification_type",
        //   id: "doc_verification_type",
        //   required: true,
        //   type: "select",
        //   data: docVerificationTypeList,
        // },
        {
          label: "Image File",
          id: "doc_verification_image_base64",
          required: true,
          type: "file",
        },
      ],
    },
    {
      title: "Sign",
      inputs: [
        // {
        //   label: "Document Type",
        //   id: "signDocumentType",
        //   required: true,
        // },
        {
          label: "Image File",
          id: "doc_sign_image_base64",
          required: true,
          type: "file",
        },
      ],
    },
  ],
];
*/
