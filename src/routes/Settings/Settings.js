import React from "react";
import { inject, observer } from "mobx-react";
import { Field, Form } from "react-final-form";

// local files
import { MyButton, MyInput, MyLoader } from "../../components";
import { settingsData, validate } from "./settings.config";
import { initialValues } from "../../assets/functions";
import "./Settings.scss";

export const Settings = inject("store")(
  observer(({ store: { settings } }) => {
    const handleChangeInput = input => e => {
      input.onChange(e);
      settings.setValue(e.target.id, e.target.value);
    };

    const handleSendProfile = () => {
      settings.sendSettings();
    };

    return (
      <main className="main">
        <div className="container">
          <div className="title">Settings</div>
          <Form
            onSubmit={handleSendProfile}
            validate={validate}
            initialValues={initialValues(settings.inputs)}
            render={({ handleSubmit }) => (
              <form onSubmit={handleSubmit} noValidate>
                {settingsData.map(item => (
                  <div className="form-item-wrap" key={item.id}>
                    <div className="h1 form-item-title">{item.title}</div>
                    {item.inputs.map((row, i) => (
                      <div className="form-row" key={i}>
                        {row.map(myInput => {
                          return (
                            <div
                              className={`form-col--${row.length}`}
                              key={myInput.id}
                            >
                              <Field name={myInput.id}>
                                {({ input, meta }) => (
                                  <MyInput
                                    /* eslint-disable-next-line react/jsx-props-no-spreading */
                                    {...input}
                                    id={myInput.id}
                                    label={myInput.label}
                                    required={myInput.required}
                                    data={myInput.data}
                                    type={myInput.type}
                                    value={settings.inputs[myInput.id]}
                                    onChange={handleChangeInput(input)}
                                    meta={meta}
                                  />
                                )}
                              </Field>
                            </div>
                          );
                        })}
                      </div>
                    ))}
                  </div>
                ))}
                <div className="form-submit-wrap">
                  <MyButton
                    className="form-submit-btn"
                    value="Update"
                    type="submit"
                  />
                </div>
              </form>
            )}
          />
          {/*
          <div className="setting-cancel-wrap">
            <div className="title">Unhappy?</div>
            <MyButton
              value="Cancel my account"
              variant="button-red"
              onClick={handleClickCancelAccount}
            />
          </div>
          */}
        </div>
        {settings.loading && <MyLoader transparent />}
      </main>
    );
  }),
);
