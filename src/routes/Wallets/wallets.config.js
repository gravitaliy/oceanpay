import { idPasswordConfirmation } from "../../assets/functions";
import {
  onValidatePassword,
  onValidatePasswordConfirmation,
} from "../../assets/utils/validator.utils";

export const validate = values => {
  const errors = {};
  onValidatePassword(errors, values, "old_password");
  onValidatePassword(errors, values, "new_password");
  onValidatePasswordConfirmation(errors, values, "new_password");
  return errors;
};

export const settingsData = [
  /*
  {
    title: "Edit User",
    inputs: [
      [
        {
          label: "E-mail",
          id: "email",
        },
        {
          label: "Current password",
          id: "password",
          type: "password",
        },
      ],
    ],
  },
*/
  {
    title: "Password",
    inputs: [
      [
        {
          label: "Current password",
          id: "old_password",
          type: "password",
        },
        {
          label: "New password",
          id: "new_password",
          type: "password",
        },
        {
          label: "Password confirmation",
          id: idPasswordConfirmation,
          type: "password",
        },
      ],
    ],
  },
].map((item, i) => ({ ...item, id: i }));
