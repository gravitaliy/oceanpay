import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { inject, observer } from "mobx-react";
import qs from "qs";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";

// local files
import "./Wallets.scss";
import Dropdown from "rc-dropdown/es";
import { DropdownWrap, Modals, MyLoader } from "../../components";
import { Setting } from "../Dashboard/components/Setting/Setting";
import { currencesData } from "../../assets/enums";

export const Wallets = inject("store")(
  observer(({ store: { wallet } }) => {
    const history = useHistory();

    const getQuery = qs.parse(history.location.search, {
      ignoreQueryPrefix: true,
    });

    const activeCoin = wallet.coins[getQuery.coin];
    const activeCoinStatic = currencesData.find(item => item.value === getQuery.coin);
    const addressList = Object.keys(activeCoin.addresses);

    const [isOpenTx, setIsOpenTx] = useState(false);
    const [isOpenAddress, setIsOpenAddress] = useState(false);
    const [activeBalance, setActiveBalance] = useState();

    useEffect(() => {
      wallet.getBalance([{ value: getQuery.coin }]);
    }, []);

    useEffect(() => {
      wallet.getTxsList(getQuery.coin);
    }, []);

    const handleCreateNewAddress = () => {
      wallet.postAddressAdd(getQuery.coin);
    };

    const handleClickTransaction = txid => () => {
      wallet.getTxInfo(getQuery.coin, txid);
      setIsOpenTx(true);
    };

    const handleCloseClickTx = () => {
      setIsOpenTx(false);
    };

    const handleClickAddress = address => () => {
      wallet.getAddress(getQuery.coin, address);
      setIsOpenAddress(true);
      setActiveBalance(activeCoin.addresses[address].confirmed);
    };

    const handleCloseClickAddress = () => {
      setIsOpenAddress(false);
      setActiveBalance(false);
    };

    const walletModal = (data) => {
      if (wallet.loadingModal) return <MyLoader />;

      return (
        <div>
          {Object.entries(data).length === 0 && <div>no info</div>}
          {(activeBalance || activeBalance === 0) && (
            <div className="wallet-modal-row">
              <div><b>balance:</b></div>
              <div className="wallet-modal-value">{activeBalance}</div>
            </div>
          )}
          {Object.entries(data).map(([ key, value ]) => (
            <div className="wallet-modal-row" key={key}>
              <div><b>{key}:</b></div>
              {typeof value !== "object" ? (
                <div className="wallet-modal-value">{value}</div>
              ) : (
                <div className="wallet-modal-value--2-level">
                  {value.map(item => {
                    return (
                      <div className="wallet-modal-value--2-level-row" key={item.address}>
                        <div>
                          <b>Address: </b>
                          <span>{item.address}</span>
                        </div>
                        <div>
                          <b>Value: </b>
                          <span>{item.value}</span>
                        </div>
                      </div>
                    )
                  })}
                </div>
              )}
            </div>
          ))}
        </div>
      );
    };

    const dropdownContentData = [
      {
        label: "Create new address",
        onClick: handleCreateNewAddress,
      },
    ].map((item, i) => ({ ...item, id: i }));

    return (
      <main className="main">
        {wallet.loading ? (
          <MyLoader />
        ) : (
          <div className="container">
            {wallet.loadingNewAddress && <MyLoader transparent />}
            <div className="title">Wallets</div>
            <div className="card-wrap">
              <div className="card-title-wrap">
                <div className="h1">{activeCoinStatic.fullLabel}</div>
                <Dropdown
                  trigger={["click"]}
                  overlay={DropdownWrap(dropdownContentData)}
                  animation="slide-up"
                  placement="bottomRight"
                >
                  <div>
                    <Setting />
                  </div>
                </Dropdown>
              </div>
              <div className="card">
                <div className="wallet-heading">
                  {/*
                  {(addressList.length !== 0 && activeAddress) ? (
                    <div className="h1">
                      <span>Current address: </span>
                      <span style={{wordBreak: "break-word"}}>{activeAddress}</span>
                    </div>
                  ) : (
                    <div className="h1">no addresses</div>
                  )}
                  */}
                  <div className="wallet-heading-name">{activeCoinStatic.label}</div>
                  <div className="h1">$ {activeCoin.balance}</div>
                </div>

                <div className="wallet-table-wrap">
                  <Tabs>
                    <TabList>
                      <Tab>Addresses</Tab>
                      <Tab>Transactions</Tab>
                    </TabList>

                    <TabPanel>
                      <div className="wallet-table">
                        {addressList.length === 0 && <div className="wallet-table-no-item">no addresses</div>}
                        {addressList.map(item => (
                          <button
                            className="default wallet-table-item"
                            type="button"
                            onClick={handleClickAddress(item)}
                            key={item}
                          >
                            {item}
                          </button>
                        ))}
                      </div>
                      <Modals
                        isOpen={isOpenAddress}
                        onCloseClick={handleCloseClickAddress}
                        innerContent={walletModal(wallet.activeAddress)}
                      />
                    </TabPanel>

                    <TabPanel>
                      <div className="wallet-table">
                        {wallet.transfers.length === 0 && <div className="wallet-table-no-item">no transactions</div>}
                        {wallet.transfers.map(item => (
                          <button
                            className="default wallet-table-item"
                            type="button"
                            onClick={handleClickTransaction(item.txid)}
                            key={item.txid}
                          >
                            {item.txid}
                          </button>
                        ))}
                      </div>
                      <Modals
                        isOpen={isOpenTx}
                        onCloseClick={handleCloseClickTx}
                        innerContent={walletModal(wallet.activeTransfer)}
                      />
                    </TabPanel>
                  </Tabs>
                </div>

                {/*
                <div className="wallet-table">
                  <div className="h1 wallet-table-title">Transactions</div>
                  {wallet.transfers.length === 0 && (
                    <div className="h2">no transactions</div>
                  )}
                  {wallet.transfers.map(item => (
                    <button
                      className="default wallet-table-item"
                      type="button"
                      onClick={handleClickTransaction(item.txid)}
                      key={item.txid}
                    >
                      {item.txid}
                    </button>
                  ))}
                  <Modals
                    isOpen={isOpen}
                    onCloseClick={handleCloseClick}
                    innerContent={walletModal()}
                  />
                </div>
                */}

              </div>
            </div>
          </div>
        )}
      </main>
    );
  }),
);
