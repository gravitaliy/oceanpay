import React, { memo } from "react";
import "./Qr.scss";

export const Qr = memo(() => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="17"
      height="17"
      x="0"
      y="0"
      enableBackground="new 0 0 80 80"
      version="1.1"
      viewBox="0 0 80 80"
      xmlSpace="preserve"
      className="qr"
    >
      <path
        d="M0 0v23.3h23.3V0H0zm16.7 16.7h-10v-10h10v10zM0 56.7V80h23.3V56.7H0zm16.7 16.6h-10v-10h10v10zM56.7 0v23.3H80V0H56.7zm16.6 16.7h-10v-10h10v10z"
        className="st0"
      />
      <path
        d="M73.3 30L73.3 50 56.7 50 56.7 56.7 80 56.7 80 30z"
        className="st0"
      />
      <path
        d="M56.7 63.3L56.7 80 63.3 80 63.3 70 73.3 70 73.3 80 80 80 80 63.3z"
        className="st0"
      />
      <path d="M30 0L30 6.7 43.3 6.7 43.3 23.3 50 23.3 50 0z" className="st0" />
      <path
        d="M43.3 30L43.3 43.3 30 43.3 30 63.3 43.3 63.3 43.3 80 50 80 50 56.7 36.7 56.7 36.7 50 50 50 50 36.7 56.7 36.7 56.7 43.3 63.3 43.3 63.3 30z"
        className="st0"
      />
      <path d="M30 70H36.7V80H30z" className="st0" />
      <path d="M13.3 43.3H23.3V50H13.3z" className="st0" />
      <path
        d="M30 13.3L30 30 0 30 0 50 6.7 50 6.7 36.7 36.7 36.7 36.7 13.3z"
        className="st0"
      />
    </svg>
  );
});
