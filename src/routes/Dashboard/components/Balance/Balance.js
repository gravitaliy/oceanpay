import React, { memo, useState } from "react";

// local files
import { MyButton, Modals, MyCopyToClipboard } from "../../../../components";
import { returnCardNumber } from "../../../../assets/utils/dashboard.utils";
import { ModalContentPin } from "../../../../components/Modals/components/ModalContentPin";
import { ModalContentTransfer } from "../../../../components/Modals/components/ModalContentTransfer";
import "./Balance.scss";

export const Balance = memo(({ cards, isKycApproved }) => {
  const activeCard = cards[0].card_info;
  // eslint-disable-next-line camelcase
  const { balance, number, pin } = cards[0].card_info;

  const [openPin, setOpenPin] = useState(false);
  const [openTransfer, setOpenTransfer] = useState(false);

  const handleToggleModalPin = () => {
    setOpenPin(!openPin);
  };

  const handleToggleModalTransfer = () => {
    setOpenTransfer(!openTransfer);
  };

  return (
    <div className="card-wrap">
      <div className="card-title-wrap">
        <div className="h1">Balance</div>
      </div>
      <div className="card card--balance">
        <div className="balance-heading">
          <div className="title balance-title">$ {balance}</div>
          {/*
          <div>
            <div>Pending Balance</div>
            <div className="h1 text-green">+ 0 $ | + 11 hkd</div>
          </div>
          */}
        </div>
        <div>Card Number</div>
        <div className="balance-card-number">
          <img
            className="balance-card-number__img"
            src={require("./assets/card.svg")}
            alt=""
          />
          {number?.length > 0 && (
            <MyCopyToClipboard
              className="h1 text-grey"
              value={returnCardNumber(number)}
              copyValue={number}
            />
          )}
        </div>
        <div className="balance-btns-wrap">
          <MyButton
            className="balance-btn"
            value="Get PIN CODE"
            icon={require("./assets/pin.svg")}
            onClick={handleToggleModalPin}
          />
          <MyButton
            className="balance-btn"
            value="Transfer"
            icon={require("./assets/transactions.svg")}
            onClick={handleToggleModalTransfer}
          />
        </div>
      </div>
      <Modals
        isOpen={openPin}
        onCloseClick={handleToggleModalPin}
        innerContent={<ModalContentPin pin={pin} />}
      />
      <Modals
        isOpen={openTransfer}
        onCloseClick={handleToggleModalTransfer}
        innerContent={<ModalContentTransfer activeCard={activeCard} isKycApproved={isKycApproved} />}
      />
    </div>
  );
});
