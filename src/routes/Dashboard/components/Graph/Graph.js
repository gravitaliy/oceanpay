import React, { memo } from "react";
import "./Graph.scss";

export const Graph = memo(() => {
  return (
    <svg
      width="22"
      height="22"
      xmlns="http://www.w3.org/2000/svg"
      x="0"
      y="0"
      enableBackground="new 0 0 80 80"
      version="1.1"
      viewBox="0 0 80 80"
      xmlSpace="preserve"
      className="graph"
    >
      <path
        d="M6.7 62.8L28.6 40.8 23.9 36.1 6.7 53.3 6.7 42.8 23.3 26.1 63.3 66.1 80 49.4 75.3 44.7 63.3 56.7 23.3 16.7 6.7 33.3 6.7 0 0 0 0 80 80 80 80 73.3 6.7 73.3z"
        className="st0"
      />
      <path
        d="M42.8 26.7L46.7 22.8 63.3 39.4 80 22.8 75.3 18 63.3 30 46.7 13.3 38 22z"
        className="st0"
      />
    </svg>
  );
});
