import React, { memo } from "react";
import Dropdown from "rc-dropdown";
import "rc-dropdown/assets/index.css";
// import clsx from "clsx";
// import { renderSign } from "../../../../assets/functions";

// local files
import { Setting } from "../Setting/Setting";
import { returnLastFourNumbers } from "../../../../assets/utils/dashboard.utils";
import { DropdownWrap, CardItem } from "../../../../components";
import "./MyCards.scss";

export const MyCards = memo(({ cards, dashboard }) => {
  const activeCard = cards[0].card_info;

  const handleUnpinCard = () => {
    dashboard.unbindCard(activeCard.number);
    dashboard.cardsList = [];
  };

  const dropdownContentData = [
    {
      label: "Unpin card",
      onClick: handleUnpinCard,
    },
  ].map((item, i) => ({ ...item, id: i }));

  return (
    <div className="card-wrap">
      <div className="card-title-wrap">
        <div className="h1">My Cards</div>
        <Dropdown
          trigger={["click"]}
          overlay={DropdownWrap(dropdownContentData)}
          animation="slide-up"
          placement="bottomRight"
        >
          <div>
            <Setting />
          </div>
        </Dropdown>
      </div>
      <div className="card card--my-card">
        <div className="my-cards__heading">
          <div className="my-card-wrap">
            <CardItem activeCard={activeCard} size="md" />
          </div>
          <div className="card-balance">
            <div>Card Balance</div>
            <div className="h1 text-grey card-balance__balance">
              {`$ ${activeCard.balance}`}
            </div>
            {/*
            <div className="card-balance__state">
              <img
                className="card-balance__img"
                src={renderSign(-100).img}
                alt=""
              />
              <div className={clsx("h1", renderSign(-100).color)}>
                {`$ ${Math.abs(-100)}`}
              </div>
            </div>
            */}
          </div>
        </div>

        <div className="my-cards__status">
          <div className="my-cards__status-item">
            <div className="my-cards__status-item__title">Card status</div>
            <div className="my-cards__status-item__value text-grey active">
              Active
            </div>
          </div>
          {/*
          <div className="my-cards__status-item">
            <div className="my-cards__status-item__title">Name Card</div>
            <div className="my-cards__status-item__value text-grey">
              {activeCard.name}
            </div>
          </div>
          */}
          <div className="my-cards__status-item">
            <div className="my-cards__status-item__title">Type Card</div>
            <div className="my-cards__status-item__value text-grey">
              MasterCard
            </div>
          </div>
          <div className="my-cards__status-item">
            <div className="my-cards__status-item__title">Currency</div>
            <div className="my-cards__status-item__value text-grey">
              {activeCard.currency}
            </div>
          </div>
        </div>

        <div className="my-cards__other-cards">
          {cards?.length > 0 &&
            cards.map(item => {
              if (item.card_info.number !== activeCard.number) {
                return (
                  <div
                    className="my-cards__other-cards-item"
                    key={item.card_info.number}
                  >
                    <div className="my-cards__other-cards-item__card">
                      {returnLastFourNumbers(item.card_info.number)}
                    </div>
                    <div className="my-cards__other-cards-item__text">
                      <div className="my-cards__other-cards-item__center">
                        <div className="my-cards__other-cards-item-title">
                          Name Card
                        </div>
                        <div className="text-grey">{item.card_info.name}</div>
                      </div>
                      <div>
                        <div className="my-cards__other-cards-item-title">
                          Card Balance
                        </div>
                        <div className="h1 text-grey">{`$ ${item.card_info.balance}`}</div>
                      </div>
                    </div>
                  </div>
                );
              }
              return null;
            })}
        </div>
      </div>
    </div>
  );
});
