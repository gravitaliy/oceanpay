import React, { useEffect, useState } from "react";
import Collapsible from "react-collapsible";
import { observer } from "mobx-react";
import clsx from "clsx";
import Dropdown from "rc-dropdown";

// local files
import {
  MyButton,
  MyChart,
  MyCopyToClipboard,
  DropdownWrap,
  QrTooltip,
} from "../../../../components";
// import { renderSign } from "../../../../assets/functions";
import { Graph } from "../Graph/Graph";
import { Qr } from "../Qr/Qr";
import { getNumberRu } from "../../../../assets/functions";
import "./Replenishment.scss";

const RenderTrigger = ({ item }) => {
  return (
    <div className="trigger">
      <img className="trigger__logo" src={item.logo} alt="" />
      <div>
        <div className="h1 trigger__title">{item.label}</div>
        <div className="trigger__price-wrap">
          <div>
            <span className="h1">{`$${getNumberRu(item.stock.price)}`}</span>
            {/* <span className={clsx("h2", renderSign(item.stock.percent).color)}>
              {`(${renderSign(item.stock.percent).sign +
                Math.abs(item.stock.percent)}%)`}
            </span> */}
          </div>
          <div className="trigger__graph-img">
            <Graph />
          </div>
        </div>
      </div>
    </div>
  );
};

const RenderTriggerSibling = ({ depositAddress, id }) => {
  const isObject =
    depositAddress === null
      ? false
      : typeof depositAddress === "function" ||
        typeof depositAddress === "object";

  const getActiveAddress = isObject
    ? Object.values(depositAddress)[0]
    : depositAddress;

  const [address, setAddress] = useState(getActiveAddress);

  const handleButtonClick = item => () => {
    setAddress(item);
  };

  return (
    <>
      <div className="trigger__btn-wrap">
        <QrTooltip id={id} qrValue={address}>
          <MyButton variant="button-qr" value="Get QR" iconRender={<Qr />} />
        </QrTooltip>
      </div>
      <div className="trigger__address-wrap h2">
        <div>
          <span>Card deposit address: </span>
          <MyCopyToClipboard value={address} />
        </div>
        {isObject && (
          <div className="trigger__address__btns-wrap">
            {Object.entries(depositAddress).map(item => (
              <MyButton
                className={clsx(
                  "trigger__address__btn",
                  item[1] === address && "active",
                )}
                variant="button-qr"
                onClick={handleButtonClick(item[1])}
                key={item[0]}
                value={item[0]}
              />
            ))}
          </div>
        )}
      </div>
    </>
  );
};

export const Replenishment = observer(({ store, cardsList, token }) => {
  const cryptocurrencyAcronymAddressPair = cardsList
    ? cardsList[0]?.card_crypto_addresses?.cryptocurrencyAcronymAddressPair
    : null;
  const [depositAddresses, setDepositAddresses] = useState(null);

  const cryptocurrencyAcronymRatePair = cardsList
    ? cardsList[0]?.card_crypto_addresses?.cryptocurrencyAcronymRatePair
    : null;
  const [ratePair, setRatePair] = useState(null);

  useEffect(() => {
    store.handleGetHistoriesAndPrice();
    // setInterval(() => store.handleGetHistoriesAndPrice(), 30000);
  }, [token]);

  useEffect(() => {
    if (cryptocurrencyAcronymAddressPair && !depositAddresses) {
      const { btc, eth, usdteth, usdtomni } = cryptocurrencyAcronymAddressPair;
      const depositAddresses = {
        btc,
        eth,
        usdt: { ERC20: usdteth, OMNI: usdtomni },
      };
      setDepositAddresses(depositAddresses);
    }
  }, [cryptocurrencyAcronymAddressPair]);

  useEffect(() => {
    if (cryptocurrencyAcronymRatePair && !ratePair) {
      const { btc, eth, usdteth } = cryptocurrencyAcronymRatePair;
      const ratePair = { btc, eth, usdteth };
      setRatePair(ratePair);
    }
  }, [cryptocurrencyAcronymRatePair]);

  const replenishment = [
    {
      logo: require("./assets/btc.png"),
      label: "BTC",
      stock: {
        price: ratePair?.btc,
        percent: store.BTC.monthChangeP,
      },
      // qrUrl: "https://cryptocompare.com/coins/btc/overview/",
      depositAddress: depositAddresses?.btc,
      labels: [...store.BTC.labels],
      data: [...store.BTC.data],
    },
    {
      logo: require("./assets/eth.png"),
      label: "ETH",
      stock: {
        price: ratePair?.eth,
        percent: store.ETH.monthChangeP,
      },
      // qrUrl: "https://cryptocompare.com/coins/eth/overview/",
      depositAddress: depositAddresses?.eth,
      labels: [...store.ETH.labels],
      data: [...store.ETH.data],
    },
    {
      logo: require("./assets/usdt.png"),
      label: "USDT",
      stock: {
        price: ratePair?.usdteth,
        percent: store.USDT.monthChangeP,
      },
      // qrUrl: "https://cryptocompare.com/coins/usdt/overview/",
      depositAddress: depositAddresses?.usdt,
      labels: [...store.USDT.labels],
      data: [...store.USDT.data],
    },
  ].map((item, i) => ({ ...item, id: i }));

  const dropdownContentData = [
    {
      label: (
        <div>
          These charts are taken from{" "}
          <a
            href="//cryptocompare.com/"
            target="_blank"
            rel="noreferrer noopener"
          >
            cryptocompare.com
          </a>{" "}
          and may differ from the current deposit price
        </div>
      ),
    },
  ].map((item, i) => ({ ...item, id: i }));

  return (
    <div className="card-wrap">
      <div className="card-title-wrap">
        <div className="h1">Replenishment</div>
      </div>
      {!!depositAddresses &&
        replenishment.map(item => (
          <Collapsible
            trigger={<RenderTrigger item={item} />}
            className="card card--trigger"
            openedClassName="card card--trigger"
            triggerTagName="div"
            triggerClassName="card-trigger-inner"
            triggerOpenedClassName="card-trigger-inner"
            contentInnerClassName="card-content-inner"
            key={item.id}
            triggerSibling={() => (
              <RenderTriggerSibling
                depositAddress={item.depositAddress}
                id={`tooltip-${item.id}`}
                // qrValue={item.qrUrl}
              />
            )}
          >
            <div className="card-content__title-wrap">
              <div className="h1">Monthly statistics</div>
              <Dropdown
                trigger={["click"]}
                overlay={DropdownWrap(dropdownContentData)}
                animation="slide-up"
                placement="bottomRight"
              >
                <div className="card-content__title-btn">
                  <img src={require("./assets/information.svg")} alt="" />
                </div>
              </Dropdown>
            </div>
            <div className="card-content__grahp">
              <MyChart
                chartData={{
                  labels: item.labels,
                  datasets: [
                    {
                      label: "Price to USD ",
                      backgroundColor: "rgba(254,236,198,0.6)",
                      borderWidth: 3,
                      borderColor: "#F6A93F",
                      hoverBorderWidth: 3,
                      hoverBorderColor: "#000",
                      data: item.data,
                    },
                  ],
                }}
              />
            </div>
          </Collapsible>
        ))}
    </div>
  );
});
