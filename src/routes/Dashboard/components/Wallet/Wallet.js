import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { inject, observer } from "mobx-react";
import qs from "qs";
import clsx from "clsx";

// local files
import "./Wallet.scss";
import { MyButton, MyLoader } from "../../../../components";
import { routesList, currencesData } from "../../../../assets/enums";

export const Wallet = inject("store")(
  observer(({ store: { wallet } }) => {
    const history = useHistory();

    useEffect(() => {
      wallet.getBalance(currencesData);
    }, []);

    const handleRedirectWallets = coin => () => {
      history.push({
        pathname: routesList.WALLETS,
        search: qs.stringify({ coin }),
      });
    };

    return (
      <div className="card-wrap">
        <div className="card-title-wrap">
          <div className="h1">My Wallets</div>
        </div>
        <div className={clsx("card", "wallet-card", wallet.loading && "loading")}>
          {wallet.loading && <MyLoader style={{ zIndex: 2 }} />}
          {currencesData.map(item => (
            <div className="wallet__other-cards-item" key={item.value}>
              <div className="wallet__other-cards-item__card">{item.label}</div>
              <div className="wallet__other-cards-item__text">
                <div>
                  <div className="wallet__other-cards-item-title">
                    Wallet Balance
                  </div>
                  <div className="h1 text-grey">{wallet.coins[item.value].balance}</div>
                </div>
              </div>
              <div className="wallet__other-cards-item__btn">
                <MyButton value="Manage" onClick={handleRedirectWallets(item.value)} />
              </div>
            </div>
          ))}
        </div>
      </div>
    );
  })
);
