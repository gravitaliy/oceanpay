import React, { useEffect } from "react";
import { inject, observer } from "mobx-react";

// local file
import { AddButton, Modals, MyTable, MyLoader } from "../../components";
import { dashboardModalNames } from "../../assets/constans/modal.const";
import { Balance, MyCards, Replenishment, Wallet } from "./components";
// import { Setting } from "./components/Setting/Setting";
import { ModalContentAdd } from "../../components/Modals/components/ModalContentAdd";
import store from "../../store";
import "./Dashboard.scss";

export const Dashboard = inject("store")(
  observer(({ store: { dashboard, auth } }) => {
    const { loading, cardsList, transactions, cardInfo } = dashboard;
    const { token } = auth;

    const openModal = () => {
      dashboard.updateModalName(dashboardModalNames.newCard);
    };

    const cardCreateHandler = inputValue => e => {
      e.preventDefault();
      dashboard.bindCard(inputValue);
    };

    const onModalCloseClick = () => {
      dashboard.updateModalName();
    };

    useEffect(() => {
      dashboard.getCardsListInfo();
    }, [token]);

    useEffect(() => {
      if (cardInfo) {
        dashboard.updateModalName();
      }
    }, [cardInfo, token]);

    useEffect(() => {
      if (cardsList?.length) {
        dashboard.getTransactionsList(cardsList[0]?.card_info?.number);
      }
    }, [cardsList, token]);

    const isKycApproved =
      !!cardsList?.length && cardsList[0]?.card_info.kyc_status === "APPROVED";

    return (
      <main className="main">
        {loading ? (
          <MyLoader />
        ) : (
          <>
            <div className="container">
              <div className="dashboard-title-wrap">
                <div className="title dashboard-title">Account</div>
                {!!cardsList?.length && !isKycApproved && (
                  <div className="h1 text-red">
                    {`You didn't pass KYC! your balance limit - (${
                      !!cardsList?.length &&
                      cardsList[0].card_permission.balance_limit
                    }$)`}
                  </div>
                )}
              </div>
              <div className="dashboard-row">
                <div className="dashboard-col-left">
                  {!!cardsList?.length && (
                    <>
                      <Balance cards={cardsList} isKycApproved={isKycApproved} />
                      <Replenishment
                        cardsList={cardsList}
                        store={store.replenishment}
                        token={token}
                      />
                    </>
                  )}
                  {!cardsList?.length && (
                    <>
                      <div className="card-wrap">
                        <div className="card-title-wrap">
                          <div className="h1">Enroll New Card</div>
                        </div>
                      </div>
                      <AddButton onClick={openModal} />
                    </>
                  )}
                </div>
                <div className="dashboard-col-right">
                  {!!cardsList?.length && (
                    <MyCards dashboard={dashboard} cards={cardsList} />
                  )}
                  <Wallet />
                  {transactions?.length && !!cardsList?.length && (
                    <div className="card-wrap">
                      <div className="card-title-wrap">
                        <div className="h1">Transactions</div>
                        {/* <Setting /> */}
                      </div>
                      <div className="card card--table">
                        <MyTable data={transactions} />
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
            <Modals
              isOpen={dashboard.modalName}
              onCloseClick={onModalCloseClick}
              innerContent={<ModalContentAdd onSubmit={cardCreateHandler} />}
            />
          </>
        )}
      </main>
    );
  }),
);
