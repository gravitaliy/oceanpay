FROM node:lts

# ARG REACT_APP_API_URL
# ARG REACT_APP_AUTH_API_URL
# ENV REACT_APP_API_URL=$REACT_APP_API_URL
# ENV REACT_APP_AUTH_API_URL=$REACT_APP_AUTH_API_URL

# RUN printenv
RUN mkdir -p /opt/dockerized_app

COPY . /opt/dockerized_app

WORKDIR /opt/dockerized_app

RUN echo $REACT_APP_API_URL > .env && echo $REACT_APP_AUTH_API_URL >> .env

RUN yarn && yarn build
RUN chmod +x docker-entrypoint.sh

ENTRYPOINT [ "./docker-entrypoint.sh" ]